from rest_framework import serializers
from location.models import Country, State, City, Location


class CountrySerializer(serializers.ModelSerializer):
    # items = serializers.SerializerMethodField('get_children_ordered')
    # customer_name =
    # queryset = Country.objects.all().prefetch_related("states")
    # def get_children_ordered(self, country):
    #     serialized_data = StateSerializer(country.items.all(), many=True, read_only=True, context=self.context)
    #     return serialized_data.data

    class Meta:
        model = Country
        fields = ('id', 'name')


class StateSerializer(serializers.ModelSerializer):
    # queryset = State.objects.all().prefetch_related("cities")
    country = CountrySerializer()

    class Meta:
        model = State
        fields = ('id', 'name', 'country')


class CitySerializer(serializers.ModelSerializer):
    state = StateSerializer()

    class Meta:
        model = City
        fields = ('id', 'name', 'state')

class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = ('id', 'tin', 'gstin')
