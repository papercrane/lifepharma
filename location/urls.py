from django.conf.urls import url, include
from location.api import location_models_list

app_name = "location"

urlpatterns = [
    # url(r'^enquiries/', enquiry_api.enquiry_list, name="enquiry_list"),
    url(r'^api/location', include([
        url(r'^$', location_models_list, name='location-models'),
    ], namespace="api")),
]
# GET api/enquiries/ => enquiry list
# GET api/enquiries/choices => enquiry choice fields
# POST api/enquiries/  => create enquiry
# PUT api/enquiries/{pk}  => update enquiry
# DELETE api/enquiries/{pk}  => delete enquiry
