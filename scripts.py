import sys
import os
import django

if __name__ == '__main__':  
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'papercraneerp.settings')

    django.setup()
    from django.core import management
    from django.apps import apps
    from todo.management.commands import deadlinecrossed
    from project.management.commands import reminders
    management.call_command('deadlinecrossed', verbosity=0, interactive=False)
    management.call_command('reminders', verbosity=0, interactive=False)
