# Create your views here.

from django.db.models import Sum, F
from company.models import Designation

#  Use json to find the designation using the designation name
def select_designation(request):
    designation_name = request.GET.get('designation_name', False)
    if designation_name:
        designations = Designation.objects.filter(name__icontains=designation_name).annotate(
            text=F('name')
        ).values('id', 'text')
        return JsonResponse(list(designations), safe=False)
    return JsonResponse(list(), safe=False)
