
from django.contrib import admin, auth
from django.contrib.auth.models import Permission
from employee.models import*

from .models import *

admin.site.register(Permission)


class DivisionInline(admin.StackedInline):
    model = Division
    extra = 0


class DepartmentAdmin(admin.ModelAdmin):
    inlines = [DivisionInline]


class DesignationAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.save()
        if 'permissions' in form.changed_data:
            for employee in obj.employee_set.all():
                employee.user.user_permissions.clear()
                for per in form.cleaned_data['permissions'].all():
                    employee.user.user_permissions.add(per)


admin.site.register(Department, DepartmentAdmin)
admin.site.register(Grade)
admin.site.register(Designation, DesignationAdmin)
admin.site.register(BasicPayDaily)
admin.site.register(ModeOfDelivery)
