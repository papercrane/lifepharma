# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-03-05 10:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import erp_core.models
import smart_selects.db_fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('location', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BasicPayDaily',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.CreateModel(
            name='Designation',
            fields=[
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('minimum_working_hours', models.IntegerField(blank=True, null=True)),
                ('department', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Department')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.CreateModel(
            name='Division',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Department')),
                ('division', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='parent_division', to='company.Division')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.CreateModel(
            name='Grade',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.CreateModel(
            name='ModeOfDelivery',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('type', models.CharField(choices=[('self', 'SELF'), ('3PL', '3PL'), ('dispatch', 'DISPATCH')], default=('self', 'SELF'), max_length=30)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.AddField(
            model_name='designation',
            name='division',
            field=smart_selects.db_fields.ChainedForeignKey(auto_choose=True, blank=True, chained_field='department', chained_model_field='department', null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Division'),
        ),
        migrations.AddField(
            model_name='designation',
            name='grade',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Grade'),
        ),
        migrations.AddField(
            model_name='designation',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='location.Location'),
        ),
        migrations.AddField(
            model_name='designation',
            name='office',
            field=smart_selects.db_fields.ChainedForeignKey(auto_choose=True, blank=True, chained_field='location', chained_model_field='location', null=True, on_delete=django.db.models.deletion.CASCADE, to='location.Office'),
        ),
        migrations.AddField(
            model_name='designation',
            name='permissions',
            field=models.ManyToManyField(blank=True, to='auth.Permission'),
        ),
        migrations.AddField(
            model_name='designation',
            name='zone',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='location.Zone'),
        ),
        migrations.AddField(
            model_name='basicpaydaily',
            name='designation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Designation'),
        ),
        migrations.AddField(
            model_name='basicpaydaily',
            name='grade',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Grade'),
        ),
    ]
