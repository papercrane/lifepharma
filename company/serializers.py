from rest_framework import serializers
from company.models import Designation


class DesignationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Designation
        fields = ('name',)
