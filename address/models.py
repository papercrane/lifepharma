import uuid

from django.db import models
from smart_selects.db_fields import ChainedForeignKey

from erp_core.models import ErpAbstractBaseModel, BaseModel


class Address(BaseModel):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # class Meta:
    #     abstract = True

    addressline1 = models.CharField("Addressline 1", max_length=200, null=True, blank=False)
    addressline2 = models.CharField("Addressline 2", max_length=200, null=True, blank=True)
    area = models.CharField(max_length=100, null=True, blank=True)
    zipcode = models.IntegerField(null=True, blank=True)

    country = models.ForeignKey('location.Country', on_delete=models.CASCADE, null=True, blank=True)

    state = ChainedForeignKey('location.State',on_delete=models.CASCADE, null=True, blank=True)

    city = ChainedForeignKey('location.City', on_delete=models.CASCADE, null=True, blank=True)


class Contact(BaseModel):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    phone = models.CharField("Phone Number", max_length=20)
    alternate_phone = models.CharField("Alternate Phone Number", max_length=20, null=True, blank=True)
