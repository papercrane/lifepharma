from rest_framework import serializers

from address.models import Address, City, Country, State
from location.serializers import CountrySerializer, CitySerializer, StateSerializer


class AddressViewSerializer(serializers.ModelSerializer):
    delivery = serializers.BooleanField(default=False, required=False)
    country = CountrySerializer()
    state = StateSerializer()
    city = CitySerializer()

    class Meta:
        model = Address
        fields = ('addressline1', 'addressline2', 'area', 'zipcode', 'country', 'state', 'city')


class AddressSerializer(serializers.ModelSerializer):
    delivery = serializers.BooleanField(default=False, required=False)
    # contact = serializers.HiddenField(default=None)

    class Meta:
        model = Address
        fields = ('id', 'addressline1', 'addressline2', 'area', 'zipcode', 'country', 'state', 'city', 'delivery', 'contact')

    def create(self, validated_data):
        return Address.objects.create(**validated_data)
