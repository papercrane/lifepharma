export default {
  getPage: {
    methods: {
      getPage(page){
        this.pagination.current = Number(page)
        this.query['page'] = page
        this.get_enquiries()
      },
    }
  }
}