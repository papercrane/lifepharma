import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// import EditEnquiry from './components/enquiry/edit.vue'
import QuotationList from './components/list.vue'
// import EnquiryDetail from './components/enquiry/detail.vue'

const EditQuotation = resolve => {
	require.ensure(['./components/edit.vue'], () => {
		resolve(require('./components/edit.vue'))
	})
}

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * @param  {string}   name     the filename (basename) of the view to load.
 */
// function view(name) {
//     return function(resolve) {
//         require(['./views/' + name + '.vue'], resolve);
//     }
// };

const routes = [
	{ path: '/quotations', component: QuotationList, name: 'quotations' },
	//{ path: '/enquiries/create', component: EditEnquiry, name: 'create-enquiry' },{ path: '/enquiries/create', component: EditEnquiry, name: 'create-enquiry' },
	{ path: '/quotations/create', component: EditQuotation, name: 'create-quotation' },
	{ path: '/quotations/:id', component: System.import('./components/detail.vue'), name: 'quotation' },
	// { path: '/:id', component: EnquiryDetail }
]

export const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes // short for routes: routes
})

// export { router }