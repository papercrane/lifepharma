from django import forms
from django.forms import ModelForm
from customer.models import Customer


class CustomerForm(ModelForm):

    class Meta:
        model = Customer
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "phone": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "address": forms.Textarea(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['name','phone','address']


