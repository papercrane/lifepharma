# Create your views here.
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.forms import inlineformset_factory
from erp_core.pagination import paginate
from customer.filters import CustomerFilter

import pprint

@login_required
def list(request):
    

    f = CustomerFilter(request.GET)

    customers, pagination = paginate(request, queryset=f.qs, per_page=2, default=True)

    context = {
        'customers': customers,
        # 'status': Customer.STATUSES,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "customer/list.html", context)


@login_required
@transaction.atomic
def create(request, enquiry_pk=None):

    ContactInlineFormSet = inlineformset_factory(Customer, Contact, form=ContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    contact_formset = ContactInlineFormSet(request.POST or None)

    customer_form = CustomerForm(request.POST or None)
   
    if request.method == "POST":

        if customer_form.is_valid() and contact_formset.is_valid():
            customer = customer_form.save()

            contacts = contact_formset.save(commit=False)
            for contact in contacts:
                contact.customer = customer
                contact.save()

            return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    context = {
        'customer_form': customer_form,
        'contact_formset': contact_formset,
        'form_url': reverse_lazy('customer:create'),
        'type': "Create"

    }
    return render(request, "customer/edit.html", context)

@login_required
def detail(request, pk):
    contact = get_object_or_404(Contact, id=pk)
    addresses = contact.addresses
    context = {
        'contact': contact,
        'addresses': addresses,
    }
    return render(request, "contact/detail.html", context)


@login_required
@transaction.atomic
def edit(request, pk=None):
    customer = get_object_or_404(Customer, id=pk)

    customer_form = CustomerForm(request.POST or None, instance=customer)

    ContactInlineFormSet = inlineformset_factory(Customer, Contact, form=ContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    contact_formset = ContactInlineFormSet(request.POST or None, instance=customer)

    
    if request.method == "POST":

        if customer_form.is_valid() and contact_formset.is_valid():
            customer_form.save()

            contact_formset.save()

            return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    context = {
        'customer_form': customer_form,
        'contact_formset': contact_formset,
        'form_url': reverse_lazy('customer:edit', args=[customer.pk]),
        'type': "Create"

    }
    return render(request, "customer/edit.html", context)

@login_required
def delete(request, pk):
    if request.method == "POST":
        customer = get_object_or_404(Customer, id=pk)
        
        customer.delete()
    return HttpResponseRedirect(reverse('customer:list'))
