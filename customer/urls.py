from django.conf.urls import include, url
from rest_framework import routers
from django.views.decorators.csrf import csrf_exempt
from customer import api, customer, contact, address,address_api

app_name = "customer"

# router = routers.DefaultRouter()
# router.register(r'customers', api.CustomerViewSet, base_name="customer")
# router.register(r'contacts', api.ContactViewSet, base_name="contact")
# m_router = routers.DefaultRouter()
# m_router.register(r'address', address_api.AddressCreateViewSet, base_name="contact_address")
# router.register(r'addresses', api.AddressViewSet, base_name="address")

# customer_list = api.CustomerViewSet.as_view({'get': 'list'})
# customer_create = api.CustomerViewSet.as_view({'get': 'create_form'})
# customer_retrieve = api.CustomerViewSet.as_view({'get': 'retrieve'})
# customer_edit = api.CustomerViewSet.as_view({'get': 'edit_form'})
# contact_retrieve = api.ContactViewSet.as_view({'get': 'retrieve'})
# contact_select_list = api.ContactViewSet.as_view({'get': 'select_list'})


urlpatterns = [
    # url(r'^$', views.index, name="list"),
    # url(r'^customers$', customer_list, name="customers"),
    # Customer urls
    url(r'^customers/$', customer.list, name="list"),
    url(r'^customers/create/$', customer.create, name="create"),
    url(r'^customers/(?P<pk>[^/]+)/transactions/$', customer.all_transactions, name="transactions"),
    url(r'^customers/(?P<pk>[^/]+)/$', customer.detail, name="detail"),
    url(r'^customers/(?P<pk>[^/]+)/edit/$', customer.edit, name="edit"),
    url(r'^customers/(?P<pk>[^/]+)/delete/$', customer.delete, name="delete"),

    # Contact Urls
    url(r'^contacts/(?P<pk>[^/]+)/$', contact.detail, name="contact-detail"),
    url(r'^customer/(?P<barcode>[^/]+)/new/create/$', customer.customer_new_create, name="customer_new_create"),


]


# GET api/enquiries/ => customer list
# GET api/enquiries/choices => customer choice fields
# POST api/enquiries/  => create customer
# PUT api/enquiries/{pk}  => update customer
# DELETE api/enquiries/{pk}  => delete customer
