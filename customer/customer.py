# Create your views here.
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from customer.models import Customer
from erp_core.pagination import paginate
from customer.filters import CustomerFilter
from customer.forms import CustomerForm
from erp_core.pagination import paginate
from asset.models import AssetTransaction
from django.db.models.functions import TruncDay
import datetime

# Customers list
@login_required
def list(request):

    f = CustomerFilter(request.GET, queryset=Customer.objects.all().order_by("-created_at"))

    customers, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)

    # print(f.form)
    context = {
        'customers': customers,
        # 'status': Customer.STATUSES,
        'paginatioinn': pagination,
        'filter': f,
    }

    return render(request, "customer/list.html", context)

#  Customers create and their contact create using inline
@login_required
@transaction.atomic
def create(request, enquiry_pk=None):


    customer_form = CustomerForm(request.POST or None)
    if request.method == "POST":

        if customer_form.is_valid() :

           customer = customer_form.save()
           return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    context = {
        'customer_form': customer_form,
        'form_url': reverse_lazy('customer:create'),
        'type': "Create"

    }
    return render(request, "customer/edit.html", context)

#  Customer detail page
@login_required
def detail(request, pk):
    customer = get_object_or_404(Customer, id=pk)
    transactions= customer.transactions.filter(parent=None).annotate(date=TruncDay('date_and_time')).values_list('date', 'destination__user__first_name').distinct()

    context = {
        'customer': customer,
        'transactions': transactions,
    }
    return render(request, "customer/detail.html", context)

#  Customer edit
@login_required
@transaction.atomic
def edit(request, pk=None):
    customer = get_object_or_404(Customer, id=pk)

    customer_form = CustomerForm(request.POST or None, instance=customer)

    if request.method == "POST":

        if customer_form.is_valid():
            customer_form.save()

            return HttpResponseRedirect(reverse('customer:detail', args=[customer.id]))

    context = {
        'customer_form': customer_form,
        'form_url': reverse_lazy('customer:edit', args=[customer.pk]),
        'type': "Edit",
        'customer':customer

    }
    return render(request, "customer/edit.html", context)

# Customer delete
@login_required
def delete(request, pk):
    if request.method == "POST":
        customer = get_object_or_404(Customer, id=pk)
        # print("delete")
        customer.delete()
    return HttpResponseRedirect(reverse('customer:list'))


@login_required
@transaction.atomic
def customer_new_create(request, barcode):
    # asset = AssetTransaction.objects.get(pk=barcode)

    customer_form = CustomerForm(request.POST or None)

    if request.method == "POST":

        if customer_form.is_valid():
            customer=customer_form.save()

            return HttpResponseRedirect(reverse('asset:asset_transaction',args=[barcode])+'?customer='+str(customer.pk) )

    context = {
        'customer_form': customer_form,
        # 'form_url': reverse_lazy('asset:edit', args=[customer.pk]),
        # 'type': "Edit",
        # 'customer':customer

    }
    return render(request, "asset/transaction.html", context)




@login_required
def all_transactions(request, pk):
    transaction_date = request.GET.get('date')

    customer = get_object_or_404(Customer, id=pk)
    transactions = customer.transactions.filter(date_and_time__date = transaction_date)
    transactions, pagination = paginate(request, queryset=transactions, per_page=20, default=True)
    tr_date = datetime.datetime.strptime(transaction_date.replace('-', ''), "%Y%m%d").date()
    context = {
        'transactions': transactions,
        'pagination': pagination,
        'transaction_date': transaction_date,
        'customer': customer,
        'tr_date': tr_date,
    }
    return render(request, "customer/transaction_detail.html", context)


