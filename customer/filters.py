import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from customer.models import Customer


class CustomerFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Customer
        fields = ['name',]