import Vue from 'vue'

Vue.config.debug = true

import Cookies from 'js-cookie'
var csrftoken = Cookies.get('csrftoken')

// var VueResource = require('vue-resource')
// Vue.use(VueResource)
import axios from 'axios'
axios.defaults.headers.common['X-CSRFToken'] = csrftoken
Vue.prototype.$http = axios

import App from './components/ListApp.vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)


new Vue({
  el: '#app',
  render: h => h(App)
})