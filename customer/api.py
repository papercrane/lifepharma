# from django.http import JsonResponse
# from django.views.decorators.csrf import ensure_csrf_cookie
# from django.utils.decorators import method_decorator
# from django.db.models import F, Prefetch, functions, Q, Value as V
# from django.urls import reverse, reverse_lazy
# from django.shortcuts import get_object_or_404
# from django.http import Http404
# # from django.db.models import Prefetch
# # from rest_framework.renderers import TemplateHTMLRenderer
# # from rest_framework.decorators import detail_route, list_route
# from rest_framework.response import Response
# from rest_framework.decorators import list_route, detail_route, api_view, permission_classes
# from rest_framework import viewsets, generics, status, mixins
# from rest_framework.renderers import JSONRenderer
# from rest_framework.views import APIView
# from customer.serializers import CustomerSerializer, CustomerContactSerializer, CustomerSelect2Serializer, ContactSerializer, AddressSerializer, AddressViewSerializer
# from customer.models import Customer, Phase, Contact, Address
#
# from location.models import Country, State, City
# from location.serializers import CountrySerializer, StateSerializer, CitySerializer
# # import json
# from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
#
#
# @ensure_csrf_cookie
# def select2_list(request):
#     name = request.GET.get('name', False)
#     if name:
#         customers = Customer.objects.filter(
#             name__contains=name
#         ).annotate(
#             text=F('name')
#         ).values('id', 'text')
#         return JsonResponse(list(customers), safe=False)
#     return JsonResponse(list(), safe=False)
#
#
# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class CustomerViewSet(viewsets.ModelViewSet):
#     # authentication_classes=()
#     # queryset = Enquiry.objects.all()
#     serializer_class = CustomerSerializer
#     # renderer_classes = (TemplateHTMLRenderer,)
#
#     def get_queryset(self):
#         # items = Prefetch('items', queryset=Item.objects.select_related('enquiry'))
#         queryset = Customer.objects.prefetch_related('contacts', 'phases').order_by('name')
#         return queryset
#
#     def list(self, request, *args, **kwargs):
#
#         response = super(CustomerViewSet, self).list(request)
#         response.data['form_url'] = reverse_lazy('customer:create')
#         response.template_name = "customer/index.html"
#         return response
#
#     def create(self, request, *args, **kwargs):
#         response = super(CustomerViewSet, self).create(request)
#         response.data['success_url'] = reverse('customer:detail', args=[response.data['id']])
#         return response
#
#     def retrieve(self, request, *args, **kwargs):
#         response = super(CustomerViewSet, self).retrieve(request)
#         # response.data['success_url'] = reverse_lazy('customer:detail')
#         response.template_name = "customer/detail.html"
#         return response
#
#     def update(self, request, *args, **kwargs):
#         response = super(CustomerViewSet, self).update(request)
#         response.data['success_url'] = reverse_lazy('customer:detail', args=[response.data['id']])
#         return response
#
#     @list_route()
#     def choices(self, request):
#         # states = Prefetch('items', queryset=State.objects.select_related('country').prefetch_related('city'))
#         # queryset = Country.objects.all().prefetch_related("states", "states__cities")
#         countries = CountrySerializer(Country.objects.all(), many=True).data
#         states = StateSerializer(State.objects.all(), many=True).data
#         cities = CitySerializer(City.objects.all(), many=True).data
#         context = {
#             "genders": Contact.GENDER,
#             "default_gender": Contact._meta.get_field('gender').get_default(),
#             "phases": Phase.PHASES,
#             "default_phase": Phase._meta.get_field('name').get_default(),
#             "countries": countries,
#             "states": states,
#             "cities": cities,
#             'form_url': reverse_lazy('customer:api:customer-list')
#         }
#         return Response(context)
#
#     @list_route()
#     def select2_list(self, request):
#         name = request.GET.get('name', None)
#         data = {}
#         if name is not None:
#             data = CustomerSelect2Serializer(Customer.objects.filter(name__contains=name), many=True).data
#         # customers = Customer.objects.filter(
#         #     name__contains=name
#         # ).annotate(
#         #     text=F('name')
#         # ).values('id', 'text')
#         # #print(customers)
#         return Response(data)
#
#     @list_route()
#     def create_form(self, request):
#
#         response = Response()
#         response.template_name = "customer/edit.html"
#         return response
#
#     @detail_route()
#     def edit_form(self, request, *args, **kwargs):
#         instance = self.get_object()
#         serializer = self.get_serializer(instance)
#         response = Response(serializer.data)
#         response.template_name = "customer/edit.html"
#         return response
#
# # class CustomerList(generics.ListAPIView):
# #     queryset = Customer.objects.all()
# #     serializer_class = CustomerViewSerializer
# #     # permission_classes = (IsAdminUser,)
#
# #     def list(self, request, *args, **kwargs):
# #         # print(CustomerViewSerializer())
# #         response = super(CustomerList, self).list(request)
# #         response.data['form_url'] = reverse_lazy('customer:customers_create')
# #         response.template_name = "customer/index.html"
# #         return response
#
#
# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class ContactViewSet(viewsets.ModelViewSet):
#     # authentication_classes=()
#     queryset = Contact.objects.all().order_by('name')
#     serializer_class = ContactSerializer
#     # renderer_classes = (TemplateHTMLRenderer,)
#     lookup_url_kwarg = 'contact_pk'
#     lookup_value_regex = '[^/]+'  # '[0-9a-f]{32}'
#
#     def retrieve(self, request, contact_pk=None, * args, **kwargs):
#         response = super(ContactViewSet, self).retrieve(request)
#         # response.data['success_url'] = reverse_lazy('contact:detail')
#         response.template_name = "contact/detail.html"
#         return response
#
#     def update(self, request, *args, **kwargs):
#         response = super(ContactViewSet, self).update(request)
#         response.data['success_url'] = reverse_lazy('contact:detail')
#         return response
#
#     def destroy(self, request, *args, **kwargs):
#         response = super(ContactViewSet, self).destroy(request)
#         return response
#
#     @list_route()
#     def choices(self, request):
#         # states = Prefetch('items', queryset=State.objects.select_related('country').prefetch_related('city'))
#         # queryset = Country.objects.all().prefetch_related("states", "states__cities")
#         countries = CountrySerializer(Country.objects.all(), many=True).data
#         states = StateSerializer(State.objects.all(), many=True).data
#         cities = CitySerializer(City.objects.all(), many=True).data
#         context = {
#             "genders": Contact.GENDER,
#             "default_gender": Contact._meta.get_field('gender').get_default(),
#             "phases": Phase.PHASES,
#             "default_phase": Phase._meta.get_field('name').get_default(),
#             "countries": countries,
#             "states": states,
#             "cities": cities,
#             'form_url': reverse_lazy('contact:api:contact-list')
#         }
#         return Response(context)
#
#     @detail_route()
#     def edit_form(self, request, *args, **kwargs):
#         instance = self.get_object()
#         serializer = self.get_serializer(instance)
#         response = Response(serializer.data)
#         response.template_name = "contact/edit.html"
#         return response
#
#     @list_route()
#     def select_list(self, request):
#         name = request.GET.get('name', None)
#         contacts = {}
#         if name is not None:
#             contacts = Contact.objects.filter(
#                 Q(name__contains=name) | Q(customer__name__contains=name)
#                 # name__contains=name
#             ).annotate(
#                 text=functions.Concat('name', V(', '), 'designation', V(', '), 'customer__name')
#             ).values('id', 'text')
#             return JsonResponse(list(contacts), safe=False)
#         return JsonResponse(list(), safe=False)
#
#
# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class AddressCreate(generics.CreateAPIView):
#     queryset = Address.objects.all()
#     serializer_class = AddressSerializer
#
#     def create(self, request, contact_pk, *args, **kwargs):
#         self.contact_pk = contact_pk
#         response = super(AddressCreate, self).create(request)
#         return response
#
#     def perform_create(self, serializer):
#         contact = get_object_or_404(Contact, id=self.contact_pk)
#         return serializer.save(contact=contact)
#
#
# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class UpdateBillinAddress(generics.UpdateAPIView):
#     queryset = Address.objects.all()
#     serializer_class = AddressSerializer
#     lookup_url_kwarg = "address_pk"
#
#     def perform_update(self, serializer):
#         contact = get_object_or_404(Contact, id=self.contact_pk)
#         return serializer.save(contact=contact)
#
#     def partial_update(self, request, contact_pk, address_pk, * args, **kwargs):
#         self.contact_pk = contact_pk
#         kwargs['partial'] = True
#         return self.update(request, *args, **kwargs)
#
#
# @api_view(['GET'])
# @permission_classes([])
# def address_choices(request):
#     countries = CountrySerializer(Country.objects.all(), many=True).data
#     states = StateSerializer(State.objects.all(), many=True).data
#     cities = CitySerializer(City.objects.all(), many=True).data
#     context = {
#         "countries": countries,
#         "states": states,
#         "cities": cities,
#         # 'form_url': reverse_lazy('contact:api:contact-list')
#     }
#     return Response(context)
#
#
# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class AddressRetrieve(generics.RetrieveAPIView):
#     queryset = Address.objects.all()
#     serializer_class = AddressViewSerializer
#     renderer_classes = (JSONRenderer, )
#
#
# class ContactOfCustomerDetail(APIView):
#     authentication_classes = (BasicAuthentication, TokenAuthentication)
#
#     """
#     Retrieve, update or delete a snippet instance.
#     """
#
#     def get_object(self, pk):
#         try:
#             return Contact.objects.get(pk=pk)
#         except Contact.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         contact = self.get_object(pk)
#         serializer = CustomerContactSerializer(contact)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         contact = self.get_object(pk)
#         serializer = CustomerContactSerializer(contact, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     # def delete(self, request, pk, format=None):
#     #     snippet = self.get_object(pk)
#     #     snippet.delete()
#     #     return Response(status=status.HTTP_204_NO_CONTENT)
