from django.conf import settings
from django.db import models
from erp_core.models import BaseModel



class Customer(BaseModel):
    code = models.CharField(max_length=100, unique=True, null=True)
    name = models.CharField(max_length=255)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    refered_by = models.ForeignKey('employee.Employee', on_delete=models.CASCADE, null=True, blank=True)
    address = models.TextField(null=True)
    phone = models.CharField(max_length=20,null=True)

    # category = models.ManyToManyField('customer.Category')

    def __str__(self):
        name = self.name
        if self.code:
            name += "(" + str(self.code) + ")"
        return name



