from sitetree.utils import item, tree


sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Customer',
            'customer:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="handshake-o",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),
    # ... You can define more than one tree for your app.
)
