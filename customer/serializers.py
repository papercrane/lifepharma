from rest_framework import serializers
from .models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    name = serializers.TimeField()
    address = serializers.CharField()
    phone = serializers.CharField()

    class Meta:
        model = Customer
        fields = ('name', 'address', 'phone', 'pk')
