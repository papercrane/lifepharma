from django import template

register = template.Library()


@register.filter
def calculateTime(arg1):
    if arg1 > 0:
        return str(int(arg1 / 3600)) + " hour:" + str(int(arg1 / 60) % 60) + " minute"
    else:
        return 0
