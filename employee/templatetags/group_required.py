from django import template

register = template.Library()


@register.filter
def group_required(user, group_name):
    """Requires user membership in at least one of the groups passed in."""
    if user.is_authenticated():
        if bool(user.groups.filter(name=group_name)) or user.is_superuser:
            return True
    return False
