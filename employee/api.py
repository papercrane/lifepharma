from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from .models import Batterystatus
from .serializers import BatterystatusSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from rest_framework.views import APIView
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.decorators import api_view, authentication_classes, permission_classes

# csrf_exempt()


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def CreateBatteryStatus(request):
    print("hi")
    print(request.user)
    Batterystatus.objects.update_or_create(employee=request.user.employee, defaults={
                                           'date_and_time': timezone.now(), 'level': request.data['level'], 'employee': request.user.employee})
    # DistanceTraveled.objects.update_or_create(employee=request.user.employee, defaults={'distance':request.data['distance'], 'date':timezone.now(), 'employee':request.user.employee})
    return Response({'status': 'ok'}, status=status.HTTP_201_CREATED)
# class CreateBatteryStatus(APIView):
#     renderer_classes = (JSONRenderer,)
#     print("hi")

#     def post(self, request, format=None):
#         print("hi")
#         Batterystatus.objects.update_or_create(employee=request.user.employee, defaults={
#                                                'date_and_time': timezone.now(), 'level': request.data['level'], 'employee': request.user.employee})
#         # DistanceTraveled.objects.update_or_create(employee=request.user.employee, defaults={'distance':request.data['distance'], 'date':timezone.now(), 'employee':request.user.employee})
#         return Response({'status': 'ok'}, status=status.HTTP_201_CREATED)


# class CreateBatteryStatus(generics.CreateAPIView):
#     # permission_classes = (AllowAny,)
#     # authentication_classes = (BasicAuthentication)
#     # permission_classes = (IsAuthenticated,)
#     # renderer_classes = (JSONRenderer, )
#     serializer_class = BatterystatusSerializer

#     # def dispatch(self, request, *args, **kwargs):
#     #     return super(CreateBatteryStatus, self).dispatch(request, *args, **kwargs)

#     def get_queryset(self):
#         employee = self.request.user.employee
#         return employee

#     def create(self, request, *args, **kwargs):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save(employee=self.request.user.employee)
#         headers = self.get_success_headers(serializer.data)
#         return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
