from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F, Q, functions, Value as V
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from guardian.shortcuts import get_objects_for_user
from location.models import *
from employee.signals import remove_reporting_officer_permissions
import datetime
# from invoice.models import Invoice, Achievements
from django.db.models import Sum
from django.contrib import messages
from erp_core.pagination import paginate
# from timesheet.models import TimeSheet
import datetime
from rest_framework.views import APIView
from .models import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
# from enquiry.models import Enquiry
from django.utils import timezone

# Employee list page
@login_required
@permission_required('employee.view_employee_list', raise_exception=True)
def index(request):
    employees = get_objects_for_user(request.user, 'employee.view_employee_details', accept_global_perms=False)
    context = {
        'employees': employees
    }
    return render(request, "employee/index.html", context)

#  Using json select the employee name 
@login_required
def select2_list(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            Q(user__username__icontains=employee_name) | Q(user__first_name__icontains=employee_name)
        ).annotate(
            text=functions.Concat('user__username', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)

# Using json select the employee name from getting the employee name
def select_employee(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            user__first_name__contains=employee_name
        ).annotate(
            text=functions.Concat('user__first_name', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        # print(employees)
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)

# Using the designation base on the office name
def select_designation(request):
    office_name = request.GET.get('office_name', False)
    if office_name:
        office = Office.objects.get(name=office_name)
        designations = Designation.objects.filter(office=office
                                                  ).annotate(
            text=('name')
        ).values('id', 'text')
        # print(employees)
        return JsonResponse(list(designations), safe=False)
    return JsonResponse(list(), safe=False)

# select the office
def select_office(request):
    office = request.GET.get("office")

    offices = list(Office.objects.filter(name__contains=office).values('id', 'name').distinct())

    if request.is_ajax():
        return JsonResponse(offices, safe=False)

#  Craete the employee 
@login_required
@permission_required('employee.add_employee', raise_exception=True)
def create(request):
    form1 = UserForm(request.POST or None)
    form2 = EmployeeForm(request.POST or None, request.FILES or None)
    form3 = AddressForEmployeeForm(request.POST or None)
    form4 = ContactForEmployeeForm(request.POST or None)

    if request.method == "POST":

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()
            if employee.reporting_officer:
                #timesheet = TimeSheet.objects.update_or_create(employee=employee, approved_by=employee.reporting_officer)
                permission = Permission.objects.get(codename="reporting_officier")
                employee.reporting_officer.user.user_permissions.add(permission)
            if employee.primary_designation:
                for pr in employee.primary_designation.permissions.all():
                    employee.user.user_permissions.add(pr)
            return HttpResponseRedirect(reverse('employee:list'))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form_url': reverse_lazy('employee:create')

    }

    return render(request, 'employee/create.html', context)

#  Edit the employee details
@login_required
@permission_required('employee.change_employee', raise_exception=True)
def edit(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    reporting_officer = employee.reporting_officer
    photo = employee.photo

    if request.method == "POST":
        # print(request.POST)
        form1 = UserEditForm(request.POST, instance=employee.user)
        form2 = EmployeeForm(request.POST, request.FILES, instance=employee)
        form3 = AddressForEmployeeForm(request.POST, instance=employee.employee_address)
        form4 = ContactForEmployeeForm(request.POST, instance=employee.employee_contact)

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():

            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            if employee.photo == "":
                employee.photo = photo
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()
            if 'reporting_officer' in form2.changed_data:
                #timesheet = TimeSheet.objects.update_or_create(employee=employee, approved_by=employee.reporting_officer)
                permission = Permission.objects.get(codename="reporting_officier")
                employee.reporting_officer.user.user_permissions.add(permission)
                if reporting_officer:
                    remove_reporting_officer_permissions(reporting_officer.user, employee)
            return HttpResponseRedirect(reverse('employee:profile',args=[employee.code]))

    else:

        form1 = UserEditForm(instance=employee.user)
        form2 = EmployeeForm(instance=employee)
        form3 = AddressForEmployeeForm(instance=employee.employee_address)
        form4 = ContactForEmployeeForm(instance=employee.employee_contact)

    context = {

        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'edit': "edit",
        'form_url': reverse_lazy('employee:edit', args=[employee.code])

    }

    return render(request, 'employee/create.html', context)

# Delete the employee
@login_required
@permission_required('employee.delete_employee', raise_exception=True)
def delete(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    # employee = Employee.objects.get(code=pk)
    employee.delete()
    return HttpResponseRedirect(reverse('employee:list'))

# Employee profile details display
@login_required
# @permission_required('employee.view_employee_details', raise_exception=True)
def profile(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    return render(request, 'employee/employee_prof_test.html', {'employee': employee})

# Add target
@login_required
# @permission_required('employee.add_tagets', raise_exception=True)
def add_targets(request, pk):

    form = TargetsForm(request.POST or None)
    if request.method == "POST":
        already_exists = Targets.objects.filter(employee=request.user.employee, from_date__range=[request.POST['from_date'], request.POST[
                                                'to_date']], to_date__range=[request.POST['from_date'], request.POST['to_date']])
        if not already_exists:
            if form.is_valid():
                target = form.save(commit=False)
                target.employee = get_object_or_404(Employee, id=pk)
                target.save()
                return HttpResponseRedirect(reverse('employee:view_targets', args=[pk]))
        else:
            messages.add_message(request, messages.INFO, 'Already exists')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    context = {
        "form": form,
        "pk": pk,
        "type": "add"
    }
    return render(request, 'targets/addtarget.html', context)

# Incentives details if the corresponding targets is there
def incentives(request, pk, from_date=None, to_date=None):
    today = datetime.date.today()
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        try:
            achievements = Targets.objects.get(from_date=from_date, to_date=to_date, employee_id=pk)

        except Targets.DoesNotExist:
            messages.add_message(request, messages.INFO, 'No Targets')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    else:
        try:
            achievements = Targets.objects.get(employee_id=pk, from_date__month=today.month,
                                               from_date__year=today.year, to_date__month=today.month, to_date__year=today.year)
        except Targets.DoesNotExist:
            messages.add_message(request, messages.INFO, 'No Targets')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    context = {
        'achievement': achievements,
        'today': today,
        'from_date': from_date,
        'to_date': to_date,
    }

    return render(request, "employee/view_incentive.html", context)

#  Incentives list based on dates
def incentive_list(request, from_date=None, to_date=None):
    today = datetime.date.today()
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        achievements = Targets.objects.filter(from_date=from_date, to_date=to_date)
    else:
        achievements = Targets.objects.filter(from_date__month=today.month, from_date__year=today.year,
                                              to_date__month=today.month, to_date__year=today.year)

    context = {
        'achievements': achievements,
        'today': today,
        'from_date': from_date,
        'to_date': to_date,
    }
    return render(request, "employee/incentive_list.html", context)

# Targets list based on employee id base
@login_required
def view_targets(request, pk):

    targets = Targets.objects.filter(employee_id=pk).order_by("-created_at")
    targets, pagination = paginate(request, queryset=targets, per_page=10, default=True)
    context = {
        "targets": targets,
        "pk": pk,
        'pagination': pagination
    }
    return render(request, 'targets/viewtargets.html', context)

# Edit target
@login_required
# @permission_required('employee.add_tagets', raise_exception=True)
def edit_targets(request, pk, target_pk):

    # target = get_object_or_404(Targets, id=target_pk)
    target = Targets.objects.get(id=target_pk)
    form = TargetsForm(request.POST or None, instance=target)
    if request.method == "POST":
        form.save()
        return HttpResponseRedirect(reverse('employee:view_targets', args=[pk]))
    context = {
        "form": form,
        'form_url': reverse_lazy('employee:edit_targets', args=[pk, target_pk]),
        "pk": pk,
        "target_pk": target_pk,
        "type": "edit"
    }
    return render(request, 'targets/addtarget.html', context)





# Timesheet list based on employee id and dates base
@login_required
def timesheet(request, pk, from_date=None, to_date=None):
    today = datetime.date.today()
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        timesheets = TimeSheet.objects.filter(date__range=[from_date, to_date], employee_id=pk)
    else:
        timesheets = TimeSheet.objects.filter(date__month=today.month, date__year=today.year, employee_id=pk)

    context = {
        'timesheets': timesheets,
        'today': today,
        'from_date': from_date,
        'to_date': to_date,
        'pk': pk

    }
    return render(request, "employee/timesheet.html", context)

# Calculete the distance travel by a salesperson for mobile view
class DistanceTraveledView(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, format=None):
        DistanceTraveled.objects.update_or_create(employee=request.user.employee,date=timezone.now(), defaults={'distance':request.data['distance'], 'date':timezone.now(), 'employee':request.user.employee})
        return Response({'status': 'ok'}, status=status.HTTP_201_CREATED)

# Report 
@login_required
def my_report(request, pk, from_date=None, to_date=None):
    today = datetime.date.today()
    enquiries_count = Enquiry.objects.filter(employee=pk)
    batteries = Batterystatus.objects.filter(employee=pk)
    invoices_count = Invoice.objects.filter(sales_person__id=pk)
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()+datetime.timedelta(1)

        enquiries_count = enquiries_count.filter(created_at__range = [from_date,to_date]).count()
        batteries = batteries.filter(date_and_time__range= [from_date, to_date]).order_by('-date_and_time')[:100]
        invoices_count = invoices_count.filter(created_at__range=[from_date, to_date]).count()
        distance = DistanceTraveled.objects.filter(employee=pk, date__range=[from_date, to_date]).aggregate(Sum('distance')) 
    else:
        enquiries_count = enquiries_count.filter(created_at__month=today.month, created_at__year=today.year,created_at__day=today.day).count()
        batteries = batteries.filter(date_and_time__month=today.month, date_and_time__year=today.year,date_and_time__day=today.day).order_by('-date_and_time')[:100]
        invoices_count = invoices_count.filter(created_at__month=today.month, created_at__year=today.year,created_at__day=today.day).count()
        try:
            distance = DistanceTraveled.objects.get(employee=pk, date=today)
        except DistanceTraveled.DoesNotExist:
            distance = 0
        from_date=0
        to_date=0

    context = {
        'enquiries_count': enquiries_count,
        'distance': distance,
        'batteries': batteries,
        'invoices_count': invoices_count,
        'pk': pk,
        'from_date':from_date,
        'to_date': to_date,
    }
    return render(request, "employee/my_report.html", context)


