from django.core.urlresolvers import reverse, reverse_lazy
from menu import Menu, MenuItem

from employee import permissions

# myaccount_children = (
#    MenuItem("Create",
#              reverse("admin:index"),
#              weight=80,
#              separator=True,
#              check=lambda request: request.user.is_superuser),
#     MenuItem("EDIT",
#              reverse("account_logout"),
#              weight=90,
#              separator=True,
#              icon="user"),

# )
# Add two items to our main menu


Menu.add_item("sidebar", MenuItem("Employee",
                               reverse("employee:list"),
                               weight=10,
                               check=permissions.can_access_employee,
                               icon="user"))
