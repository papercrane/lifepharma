from django.conf.urls import include, url

from employee import views, api

app_name = "employee"


urlpatterns = [
    url(r'^employee/', include([
        url(r'^$', views.index, name="list"),
        url(r'^select_list/$', views.select2_list, name="select_list"),
        url(r'^select_employee/$', views.select_employee, name="select_employee"),
        url(r'^select_designation/$', views.select_designation, name="select_designation"),
        url(r'^create/$', views.create, name='create'),
        url(r'^(?P<pk>[^/]+)/edit/', views.edit, name='edit'),
        url(r'^(?P<pk>[^/]+)/delete/', views.delete, name='delete'),
        url(r'^(?P<pk>[^/]+)/profile/', views.profile, name='profile'),
        url(r'^incentive/list/$', views.incentive_list, name='incentive_list'),
        url(r'^incentive/list/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', views.incentive_list, name="incentiveList"),
        url(r'^(?P<pk>[^/]+)/incentive/$', views.incentives, name='incentives'),
        url(r'^(?P<pk>[^/]+)/incentive/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', views.incentives, name="incentives"),
        url(r'^findoffice/', views.select_office, name="select_office"),
        # url(r'^(?P<employee_pk>[^/]+)/todo/', include('todo.urls', namespace="todo")),
        url(r'^(?P<pk>[^/]+)/addtargets/', views.add_targets, name='add_targets'),
        url(r'^(?P<pk>[^/]+)/targets/', views.view_targets, name='view_targets'),
        url(r'^(?P<pk>[^/]+)/editTarget/(?P<target_pk>[^/]+)/$', views.edit_targets, name='edit_targets'),
        url(r'^batterystatus/create/$', api.CreateBatteryStatus, name="CreateBatteryStatus"),
        url(r'^(?P<pk>[^/]+)/timesheet/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$', views.timesheet, name='timesheet_withdate'),
        url(r'^(?P<pk>[^/]+)/timesheet/', views.timesheet, name='timesheet'),
        url(r'^(?P<pk>[^/]+)/report/$', views.my_report, name="my_report"),
        url(r'^(?P<pk>[^/]+)/report/(?P<from_date>[-\w]+)/(?P<to_date>[-\w]+)/$',
            views.my_report, name="my_report_with_range"),


    ])),
    url(r'^api/employee/select_list/$', views.select2_list, name="select_list"),

]
