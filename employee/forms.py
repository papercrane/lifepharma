from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Targets
from location.models import *
from employee.models import Employee, AddressForEmployee, ContactForEmployee
# from django.core.exceptions import NON_FIELD_ERRORS


class UserForm(UserCreationForm):
  class Meta:
    model = User
    widgets = {"username": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
               "email": forms.TextInput(attrs={'class': "form-control1", 'type': "email", 'required': "required"}),
               "first_name": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
               "password1": forms.TextInput(attrs={'class': "form-control1", 'id': "exampleInputPassword1", 'placeholder': "Password"}),
               "password2": forms.TextInput(attrs={'class': "form-control1", 'required': "required", 'placeholder': "Password"}),
               }
    fields = ['username', 'email', 'first_name', 'password1', 'password1']


class UserEditForm(UserChangeForm):
  class Meta:
    model = User
    widgets = {"username": forms.TextInput(attrs={'class': "form-control1"}),
               "email": forms.TextInput(attrs={'class': "form-control1", 'type': "email"}),
               "first_name": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
               "password": forms.TextInput(attrs={'class': "form-control1"}),
               }
    fields = ['username', 'email', 'first_name', 'password']


class EmployeeForm(ModelForm):

  reporting_officer = forms.ModelChoiceField(
      required=False,
      label="Reports To",
      queryset=Employee.objects.none(),
      widget=forms.Select(attrs={'class': 'form-control', 'width': "100%"}),
  )

  class Meta:
    model = Employee
    widgets = {"code": forms.TextInput(attrs={'class': "form-control1", 'required': "required", 'id': "code"}),
               "dob": forms.DateInput(attrs={'class': "form-control1", "id": "pldate"}),
               "gender": forms.Select(attrs={'class': "form-control1", "data-toggle": "select"}),
               "marital_status": forms.Select(attrs={'class': "form-control1", "data-toggle": "select"}),
               "blood_group": forms.Select(attrs={'class': "form-control1", "data-toggle": "select"}),
               "languages_known": forms.TextInput(attrs={'class': "form-control1"}),
               # "birth_place": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
               "citizenship": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
               # "cast": forms.TextInput(attrs={'class': "form-control1", }),
               # "religion": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
               "photo": forms.FileInput(attrs={'class': "form-control1"}),
               "primary_designation": forms.Select(attrs={'class': "form-control1", "data-toggle": "select", 'required': "required"}),
               "secondary_designation": forms.SelectMultiple(attrs={'class': "form-control1", "data-toggle": "select", "id": "secondary_desig", 'multiple': "TRUE"}),
               #"reporting_officer": forms.SelectMultiple(attrs={'class': "form-control1", "data-toggle": "select",'multiple': "TRUE"}),

               }
    fields = ['code', 'dob', 'gender', 'marital_status', 'blood_group', 'languages_known', 
              'citizenship','photo', 'primary_designation', 'secondary_designation', 'reporting_officer']

  def __init__(self, *args, **kwargs):
    # manager_qs = kwargs.pop('manager_qs')
    super(EmployeeForm, self).__init__(*args, **kwargs)
    self.set_reporting_officer_queryset()

  def set_reporting_officer_queryset(self):
    # print(self.data)
    if self.instance.id:
      self.fields['reporting_officer'].queryset = Employee.objects.filter(id=self.instance.reporting_officer_id)
    if self.data:
      if self.data['reporting_officer']:
        self.fields['reporting_officer'].queryset = Employee.objects.filter(id=self.data['reporting_officer'])


class UserChangeForm(UserChangeForm):
  class Meta:
    model = User
    fields = ('username', 'email', 'password', 'first_name')


class AddressForEmployeeForm(ModelForm):
  class Meta:
    model = AddressForEmployee

    widgets = {"addressline1": forms.TextInput(attrs={'class': "form-control1", }),
               "addressline2": forms.TextInput(attrs={'class': "form-control1"}),
               "area": forms.TextInput(attrs={'class': "form-control1", }),
               "zipcode": forms.NumberInput(attrs={'class': "form-control1", }),
               "city": forms.Select(attrs={'class': "chained form-control1", }),
               "state": forms.Select(attrs={'class': "chained form-control1", }),
               "country": forms.Select(attrs={'class': "chained form-control1",}),
               }
    fields = ['addressline1', 'addressline2', 'area', 'zipcode', 'city', 'state', 'country']


class ContactForEmployeeForm(ModelForm):
  class Meta:
    model = ContactForEmployee

    widgets = {"phone": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),
               "alternate_phone": forms.NumberInput(attrs={'class': "form-control1", }),
               }
    fields = ['phone', 'alternate_phone']


class TargetsForm(ModelForm):
  class Meta:
    model = Targets

    widgets = {

        "amount": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),
        "from_date": forms.DateInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
        "to_date": forms.DateInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
        "percentage": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),

    }
    fields = ['amount', 'from_date', 'to_date', 'percentage']
    # error_messages = {
    #     NON_FIELD_ERRORS: {
    #         'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
    #     }
    # }
