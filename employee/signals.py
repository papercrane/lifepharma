from django.shortcuts import get_object_or_404
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from employee.models import Employee
from guardian.shortcuts import assign_perm, remove_perm


@receiver(post_save, sender=Employee)
def assign_reporting_officer_permissions(sender, instance=None, created=False, **kwargs):
    employee = instance
    #print(employee)
    if employee.reporting_officer:
        #print(employee.reporting_officer)
        reporting_officer = employee.reporting_officer.user
        assign_perm('todo.view_todo_list', reporting_officer)
        assign_perm('todo.add_todo', reporting_officer)
        assign_perm('employee.view_employee_list', reporting_officer)
        assign_perm('employee.view_employee_details', reporting_officer, employee)
        # assign_perm('todo.view_employee_details', reporting_officer, employee)


def remove_reporting_officer_permissions(reporting_officer, employee):
    if employee.reporting_officer:
        remove_perm('employee.view_employee_details', reporting_officer, employee)

