from autoslug import AutoSlugField
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from address.models import *
from company.models import *
from erp_core.models import ErpAbstractBaseModel
from location.models import Location
from company.models import BasicPayDaily
from django.core.validators import MinValueValidator


class AddressForEmployee(Address):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_address")

    def __str__(self):
        return str(self.employee)


class ContactForEmployee(Contact):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_contact")

    def __str__(self):
        return str(self.employee)


class Employee(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    MARITALSTATUS = (
        ('Single', 'Single'),
        ('Married', 'Married'),
    )

    BLOODGROUP = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
    )
    code = models.CharField(max_length=255, unique=True)
    photo = models.FileField(upload_to='prof/%Y/%m/%d', blank=True, null=True)
    dob = models.DateField()
    gender = models.CharField(max_length=8, choices=SEX)
    marital_status = models.CharField(max_length=10, choices=MARITALSTATUS)
    blood_group = models.CharField(max_length=3, choices=BLOODGROUP)
    languages_known = models.CharField(max_length=255)
    birth_place = models.CharField(max_length=100, blank=True, null=True)
    cast = models.CharField(max_length=100, null=True, blank=True)
    religion = models.CharField(max_length=100)
    citizenship = models.CharField(max_length=100)
    status = models.BooleanField(default=True)

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)

    primary_designation = models.ForeignKey(
        Designation, on_delete=models.CASCADE)

    secondary_designation = models.ManyToManyField(
        Designation, related_name="employee_secondary_designation", blank=True)

    reporting_officer = models.ForeignKey(
        'employee.Employee', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        permissions = (
            ("view_employee_list", "Can view employee list"),
            ("view_employee_details", "Can view employee details"),
            ("can_access", "Can access Employee Section"),
            ("reporting_officier", "I am a reporting officier"),
            ("is_support","Support")
        )

    def get_absolute_url(self):
        return reverse('employee:profile', args=[self.code])

    def __str__(self):
        return str(self.user.first_name) + " (" + str(self.code) + ")"


class EmployeeCode(ErpAbstractBaseModel):

    character_set = models.CharField(max_length=100)


class Targets(ErpAbstractBaseModel):
    amount = models.DecimalField(max_digits=50, decimal_places=2)
    employee = models.ForeignKey(Employee)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    incentive_amount = models.DecimalField(
        max_digits=200, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(0)])
    percentage = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    def __str__(self):
        return str(self.employee) + "-" + " from " + str(self.from_date) + " to " + str(self.to_date)

    # class Meta:
    #     unique_together = ("from_date", "to_date")

    # def unique_error_message(self, model_class, unique_check):
    #     print(model_class)
    #     import pdb; pdb.set_trace()
    #     if model_class == type(self) and unique_check == ("employee", "from_date", "to_date"):
    #         return 'My custom error message'
    #     else:
    #         return super(Targets, self).unique_error_message(model_class, unique_check)

# class Incentive(ErpAbstractBaseModel):
#     target = models.OneToOneField(Targets,related_name='target_incentive')
#     def __str__(self):
#         return str(self.target)


class Batterystatus(ErpAbstractBaseModel):
    employee = models.ForeignKey(Employee)
    date_and_time = models.DateTimeField()
    level = models.PositiveIntegerField()

    def __str__(self):
        return str(self.employee)


class Bonus(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, unique=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])

    def __str__(self):
        return str(self.name)


class DistanceTraveled(models.Model):
    distance = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])
    date = models.DateField()
    employee = models.ForeignKey(Employee)

    class Meta:
        unique_together = ("date", "employee")

    def __str__(self):
        return str(self.employee)
