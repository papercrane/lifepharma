import random
import json

from django.db.models import Count, F
from django.utils import timezone

from django.contrib.auth.decorators import login_required, permission_required

from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models.functions import TruncDate, datetime
from django.core import serializers

from asset.forms import AssetForm, TransactionForm
from django.shortcuts import get_object_or_404, render, redirect
from django.http import JsonResponse, HttpResponse

from asset.serializers import TransactionSerializer
from customer.forms import CustomerForm
from customer.serializers import CustomerSerializer
from erp_core.pagination import paginate
from django.http import HttpResponseRedirect
from asset.filters import AssetFilter
from django.db import transaction
from .models import *
from django.db.models.functions import TruncDate


@login_required
def dashboard(request):
    return render(request, "asset/dashboard.html")


@login_required
def stock_dashboard(request):
    return render(request, "asset/stock_dashboard.html")


@login_required
def category_dashboard(request):
    return render(request, "asset/category_dashboard.html")


@login_required
def index(request):
    f = AssetFilter(request.GET)
    assets, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
    context = {
        'assets': assets,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "asset/index.html", context)


@login_required
def create(request):
    assetform = AssetForm(request.POST or None)
    if request.method == "POST":

        if assetform.is_valid():
            asset = assetform.save(commit=False)
            random_number = rand_function()
            asset.code = random_number
            asset.save()
            return HttpResponseRedirect(reverse('asset:detail', args=[asset.pk]))

    context = {
        'assetform': assetform,
        'form_url': reverse_lazy('asset:create'),
        'type': "add",

    }

    return render(request, "asset/create.html", context)


@login_required
def customer_asset_create(request):
    assetform = CustomerAssetForm(request.POST or None)
    if request.method == "POST":

        if assetform.is_valid():
            asset = assetform.save(commit=False)
            asset.customer_asset = True
            asset.save()
            return HttpResponseRedirect(reverse('asset:detail', args=[asset.pk]))

    context = {
        'assetform': assetform,
        'form_url': reverse_lazy('asset:customer_asset_create'),
        'type': "add",
        'customer': "customer",

    }

    return render(request, "asset/customer_asset_create.html", context)


@login_required
def customer_asset_edit(request, pk):
    asset = get_object_or_404(Asset, pk=pk)
    assetform = AssetForm(request.POST or None, instance=asset)
    if request.method == "POST":

        if assetform.is_valid():
            asset.save()
            return HttpResponseRedirect(reverse('asset:list'))

    context = {
        'assetform': assetform,
        'form_url': reverse_lazy('asset:customer_asset_edit', args=[asset.pk]),
        'type': "edit",
        'customer': "customer",

    }

    return render(request, "asset/customer_asset_create.html", context)


@login_required
def edit(request, pk):
    asset = get_object_or_404(Asset, pk=pk)
    assetform = AssetForm(request.POST or None, instance=asset)
    if request.method == "POST":

        if assetform.is_valid():
            asset.save()

            return HttpResponseRedirect(reverse('asset:detail', args=[asset.pk]))

    context = {
        'assetform': assetform,
        'form_url': reverse_lazy('asset:edit', args=[asset.pk]),
        'type': "edit",
        'asset': asset,

    }

    return render(request, "asset/create.html", context)


@login_required
def detail(request, pk):

    asset = get_object_or_404(Asset, pk=pk)
    transactions=asset.asset.all()
    todays_transactions=transactions.filter(date_and_time__date=timezone.now().date())
    upcoming_transactions=todays_transactions.filter(date_and_time__gte=timezone.now())


    context = {
        'asset': asset,
        'transactions':transactions,
        'todays_transactions':todays_transactions,
        'upcoming_transactions':upcoming_transactions
    }

    return render(request, "asset/detail.html", context)


def delete(request, pk):
    if request.method == 'POST':
        asset = get_object_or_404(Asset, pk=pk)
        asset.delete()
        return HttpResponseRedirect(reverse('asset:list'))


@transaction.atomic
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            return redirect(reverse('asset:create'))

    context = {
        'code_form': code_form,
    }

    return render(request, "asset/code_setting.html", context)


def rand_function():
    random_number = random.randint(10000, 10000000000000000000)
    random_exist = Asset.objects.filter(code=random_number)
    if random_exist:
        rand_function()
    else:
        return random_number


@transaction.atomic
def asset_transaction(request, barcode):

    if request.GET.get('customer'):
        customer = get_object_or_404(Customer, pk=request.GET.get('customer'))
        assetform = TransactionForm(request.POST or None, initial={'customer': customer})
    else:
        assetform = TransactionForm(request.POST or None)
    customerform = CustomerForm(request.POST or None)
    asset = get_object_or_404(Asset, code=barcode)
    if request.method == "POST":


        if assetform.is_valid():
            transaction = assetform.save(commit=False)
            transaction.asset = asset
            transaction.created_by = request.user.employee
            transaction.updated_by = request.user.employee
            transaction.save()
            return HttpResponseRedirect(reverse('asset:transaction_detail', args=[transaction.pk]))

    context = {
        'assetform': assetform,
        'customerform': customerform,
        'type': "add",
        'barcode': barcode
    }

    return render(request, "asset/transaction.html", context)


def transaction_detail(request, pk):
    transaction = get_object_or_404(AssetTransaction, pk=pk)

    context = {
        'transaction': transaction,

    }

    return render(request, "transaction/detail.html", context)


def customer_transaction(request, selectedDate=False):
    customers = Customer.objects.filter(transactions__source=request.user.employee)

    if selectedDate:


        customers = customers.filter(transactions__date_and_time__date=selectedDate).distinct()
        serialized_entries = CustomerSerializer(customers, many=True)
        return JsonResponse(serialized_entries.data, safe=False)

    else:
        customers = customers.filter(transactions__date_and_time__date=datetime.datetime.today()).distinct()
        serialized_entries = CustomerSerializer(customers, many=True)

    context = {
        "entries": serialized_entries.data,
        "date":selectedDate,
    }

    return render(request, "transaction/emplo_test.html", context)


def customer_asset(request, date, customer):

    assets = AssetTransaction.objects.filter(source=request.user.employee, date_and_time__date=date,
                                             customer__pk=customer)

    context = {
        'assets': assets,
        'date': date

    }

    return render(request, "transaction/employee_assets.html", context)


@transaction.atomic
@login_required
def changeStatus(request, pk):
    asset = get_object_or_404(AssetTransaction, pk=pk)
    status = request.POST.get("status")
    if status == "Used" or status == "Not Used":
        asset.usage_status = status
        if status == "Used" and asset.asset.returnable == False:
            asset.asset.used = True

    if status == "Damage" or status == "Destroyed":
        asset.asset_status = status

    asset.save()
    asset.asset.save()

    return HttpResponse(asset)


@transaction.atomic
@login_required
def employee_assets(request):
    employee = ""
    employees = Employee.objects.all()
    employee_pk = request.GET.get('employee')
    date = request.GET.get('date')
    assets = AssetTransaction.objects.filter(asset__available=False,asset__used=False)

    if employee_pk:
        employee = get_object_or_404(Employee, pk=employee_pk)
        assets = assets.filter(source=employee)

    if date:
        assets = assets.filter(date_and_time__date=date)
        selected_date = date

    else:
        assets = assets.filter(date_and_time__date=timezone.now().date())
        selected_date = timezone.now().date()

    context = {
        'employees': employees,
        'assets': assets,
        'selected_date': selected_date,
        'employee': employee

    }
    return render(request, "transaction/assets.html", context)


@transaction.atomic
@login_required
def return_asset(request, pk):
    asset = get_object_or_404(Asset, pk=pk)
    asset.available = True
    asset.save()
    return HttpResponse(asset)
