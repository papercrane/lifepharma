from django.conf.urls import include, url
from . import stock, item


stock_patterns = ([
    url(r'^create/$', stock.create, name='create'),
    url(r'^code/settings/$', stock.code_settings, name='code_settings'),
    url(r'^(?P<stock_pk>[^/]+)/', include([
        url(r'^detail/$', stock.detail, name='stock_detail'),
        url(r'^edit/$', stock.edit, name='stock_edit'),
        url(r'^changeStatus/$', stock.edit_status, name='editStockStatus'),
        url(r'^items/', include([
            # url(r'^add/$', item.create, name='create'),
            url(r'^(?P<item_pk>[^/]+)/delete/$', item.delete, name='delete_item'),
            url(r'^(?P<item_pk>[^/]+)/changeStatus/$', item.edit_status, name='edit-status'),
            url(r'^(?P<item_pk>[^/]+)/edit/$', item.edit, name='edit'),
            url(r'^(?P<item_pk>[^/]+)/item_detail/$', item.item_detail, name='item_detail'),
            url(r'^', item.items, name='list'),
        ], namespace="item")),
    ])),

    # url(r'^batches/', include([
    #     url(r'^(?P<batch_pk>[^/]+)/', include([
    #         url(r'^changeStatus/$', batch.edit_status, name='edit-status'),
    #         url(r'^delete/$', batch.delete, name='delete'),
    #         url(r'^edit/$', batch.edit, name='edit'),

    #         url(r'^items/', include([
    #             url(r'^add/$', item.create, name='create'),
    #             url(r'^delete/$', item.delete, name='delete_item'),
    #             url(r'^(?P<item_pk>[^/]+)/changeStatus/$', item.edit_status, name='edit-status'),
    #             url(r'^(?P<item_pk>[^/]+)/edit/$', item.edit, name='edit'),
    #             url(r'^(?P<item_pk>[^/]+)/item_detail/$', item.item_detail, name='item_detail'),
    #             url(r'^', item.items, name='list'),
    #         ], namespace="item")),
    #     ])),
    # ], namespace="batch")),

    # url(r'^stock/batch/(?P<batch_pk>[^/]+)/edit/$', stock.edit_batch, name='edit_batch'),
    # url(r'^stock/batch/item/(?P<item_pk>[^/]+)/edit/$', stock.edit_item, name='edit_item'),

    url(r'^products/checkStock/$', stock.checkStock, name='checkStock'),
    url(r'^asset/lists/$', item.assetLists, name='assetLists'),
    url(r'^asset/staffAssetlists/$', item.staff_asset_lists, name='staff_asset_lists'),


    # # For mobile view The below links
    # # search for stock via product code- Get
    # url(r'^batch/product/(?P<code>[\w\-]+)/$', batch.batch_detail),
    # url(r'^batch/(?P<batch_pk>[^/]+)/statusBooked/$', batch.batch_status_booked),
    # url(r'^batch/(?P<batch_pk>[^/]+)/cancelBooked/$', batch.batch_cancel_booked),
    url(r'^', stock.index, name="list"),
])
