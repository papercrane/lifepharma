from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F, Q, functions, Value as V
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.forms import inlineformset_factory, modelform_factory
from globalconfig.globalconfig import isInventoryPO, isOnlyInventory
from .forms import *
from django.utils import timezone
from django.shortcuts import get_object_or_404, render, redirect
from erp_core.pagination import paginate
from asset.purchase_order.models import PurchaseOrder
import pprint
from .filters import StockFilter, ItemFilter
from .models import StockItem as Item
from asset.models import Asset as asset
# from inventory.serializers import BatchSerializer
from django.contrib import messages


def add_items(request, batch_pk):
    pass

    # batch = get_object_or_404(Batch, id=batch_pk)
    # ItemInlineFormSet = inlineformset_factory(
    #     Batch, Item, form=ItemForm, extra=0, can_delete=True, min_num=1, max_num=(batch.received_quantity - batch.asset_stock_item_set.all().count()), validate_min=True)
    # items_formset = ItemInlineFormSet(request.POST or None)

    # if request.method == "POST":
    #     if items_formset.is_valid():

    #         items = items_formset.save(commit=False)

    #         for item in items:
    #             item.batch = batch
    #             # item.status = batch.status
    #             item.save()

    #         return redirect(reverse('inventory:asset:stock:batch:item:list', args=[batch.pk]))
    # context = {
    #     "batch": batch,
    #     'items_formset': items_formset,
    #     'form_url': reverse_lazy('inventory:asset:stock:batch:add-items', args=[batch.pk]),
    #     "type": "add_items"
    # }

    # return render(request, 'asset/stock/add_items.html', context)


def items(request, batch_pk):
    # batch = get_object_or_404(Batch, id=batch_pk)
    # left_items = batch.received_quantity - Item.objects.filter(batch=batch).count()
    # f = ItemFilter(request.GET, queryset=Item.objects.filter(batch=batch).order_by("-created_at"))
    # items = f.qs
    # context = {
    #     'batch': batch,
    #     'items': items,
    #     'left_items': left_items,
    #     'filter': f,
    # }
    # return render(request, "asset/stock/items_detail.html", context)
    pass


def edit_status(request, batch_pk):
    pass

    # new_status = request.POST['status']

    # # manual change of status to
    # # "sold","blocked","booked","BLOCKING_REQUEST","BOOKED" is not possible in
    # # # quotation or invoice is there
    # # if not isOnlyInventory():
    # #     messages.add_message(request, messages.ERROR, "You can't change the batch status manually to " + new_status)
    # #     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    # # get the batch and change its status
    # batch = Batch.objects.get(id=batch_pk)
    # prev_status = batch.status
    # batch.status = new_status
    # batch.save()

    # # if there is items in batch - items status must be also changed according to batch status
    # for item in batch.asset_stock_item_set.all():
    #     item.status = batch.status
    #     item.save()

    # # updating the total inventory

    # if prev_status == batch.status or (prev_status == Batch.INTRANSIT and batch.status == Batch.INSTOCK) and (prev_status == Batch.INSTOCK and batch.status == Batch.INTRANSIT):
    #     pass

    # elif (prev_status == Batch.INTRANSIT and new_status == Batch.PENDING) or (prev_status == Batch.INSTOCK and new_status == Batch.PENDING):
    #     total_inventory = Total_Inventory.objects.get(asset=batch.asset, location=batch.stock.location)
    #     total_inventory.current_stock = total_inventory.current_stock - batch.current_quantity
    #     total_inventory.save()

    # elif (prev_status == Batch.PENDING and new_status == Batch.INTRANSIT) or (prev_status == Batch.PENDING and new_status == Batch.INSTOCK):
    #     # go to edit batch - we need to update recieved quantity and corresponding calculation in total inventory
    #     return redirect(reverse('asset:stock:batch:edit', args=[batch.pk]))

    # elif (prev_status == Batch.INTRANSIT and new_status == Batch.SOLD) or (prev_status == Batch.INSTOCK and new_status == Batch.SOLD):
    #     total_inventory = Total_Inventory.objects.get(asset=batch.asset, location=batch.stock.location)
    #     total_inventory.current_stock = total_inventory.current_stock - batch.current_quantity
    #     total_inventory.save()
    #     batch.current_quantity = 0
    #     batch.stock.current_batches = batch.stock.current_batches - 1
    #     batch.stock.save()
    #     if batch.stock.current_batches == 0:
    #         batch.stock.status = Stock.SOLD
    #         batch.stock.save()
    #     batch.save()
    # else:
    #     pass
    # return redirect(reverse('asset:stock:stock_detail', args=[batch.stock.id]))


def editItemStatus(request, batch_pk, item_pk):
    pass
    # new_status = request.POST['status']
    # if not isOnlyInventory() and (new_status == "sold" or new_status == "booked"):
    #     messages.add_message(request, messages.ERROR, "You cann't change the item status manually to " + new_status)
    #     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    # item = Item.objects.get(id=item_pk)
    # prev_status = item.status

    # item.status = new_status
    # item.save()

    # if item.status == prev_status:
    #     pass
    # else:
    #     if item.status == "sold":
    #         #  Batch Current quantity is change by 1
    #         item.batch.current_quantity = item.batch.current_quantity - 1
    #         item.batch.save()

    #         if item.batch.current_quantity == 0:
    #             item.batch.status = Batch.SOLD
    #             total_inventory = Total_Inventory.objects.get(
    #                 asset=item.batch.asset, location=item.batch.stock.location)
    #             total_inventory.current_stock = total_inventory.current_stock - item.batch.current_quantity
    #             total_inventory.save()
    #             item.batch.stock.current_batches = item.batch.stock.current_batches - 1
    #             if item.batch.stock.current_batches == 0:
    #                 item.batch.stock.status = Stock.SOLD
    #                 item.batch.stock.save()
    #         item.save()

    # return HttpResponseRedirect(reverse('asset:stock:batch:item:list', args=[item.batch.id]))


def checkStock(request):
    # print("Start........")
    pass

    # assets = asset.objects.filter()
    # if request.method == "POST":
    #     current_asset = request.POST.get('asset')
    #     if current_asset:
    #         batches = Batch.objects.filter(asset_id=current_asset)
    #         count_instock = batches.filter(status=Batch.INSTOCK).count()
    #         count_intransit = batches.filter(status=Batch.INTRANSIT).count()
    #         count_booked = batches.filter(status=Batch.BOOKED).count()

    #     stocks = PurchaseOrderItems.objects.filter(asset_id=current_asset, expected_date__gte=timezone.now().date()).values(
    #         "expected_date", "asset__name", "ordered_quantity")
    #     context = {
    #         'assets': assets,
    #         'count_instock': count_instock,
    #         'count_booked': count_booked,
    #         'count_intransit': count_intransit,
    #         'stocks': stocks,
    #         'current_asset': current_asset,
    #     }
    # else:
    #     context = {
    #         'assets': assets,
    #     }

    # return render(request, "asset/stock/checkStock.html", context)


# Mobile View


# @csrf_exempt
# def batch_detail(request, code):
#     """
#     Retrieve code batch.
#     """
#     batches = Batch.objects.filter(asset__code=code)
#     count = batches.count()
#     serializer = BatchSerializer(batches, many=True)
#     return JsonResponse(serializer.data, safe=False)


# @csrf_exempt
# @login_required
# def batch_status_booked(request, batch_pk):
#     """
#     Update status to booked in the batch.
#     """
#     try:
#         batch = Batch.objects.get(pk=batch_pk)
#         batch.booked(employee=request.user.employee).save()
#         # print("batch..............",batch)
#         # change the status of item in inventory when all batches status is booked
#         items = Item.objects.filter(batch=batch)
#         for item in items:
#             item.status = Batch.BOOKED
#             item.save()
#     except Batch.DoesNotExist:
#         return JsonResponse(status=404)

#     return JsonResponse({}, status=200)


# @csrf_exempt
# def batch_cancel_booked(request, batch_pk):
#     """
#    cancel booked status.
#     """
#     try:
#         batch = Batch.objects.get(pk=batch_pk)
#         batch.cancelbooked().save()
#         items = Item.objects.filter(batch=batch)
#         for item in items:
#             item.status = Batch.INSTOCK
#             item.save()
#     except Batch.DoesNotExist:
#         return JsonResponse(status=404)

#     return JsonResponse({}, status=200)


@transaction.atomic
def delete(request, batch_pk):
    pass

    # batch = Batch.objects.get(pk=batch_pk)
    # if batch.status == Batch.INSTOCK or batch.status == Batch.INTRANSIT:
    #     total_inventory = Total_Inventory.objects.get(asset=batch.asset_id, location=batch.stock.location)
    #     total_inventory.current_stock = total_inventory.current_stock - batch.received_quantity
    #     total_inventory.save()
    # batch.delete()
    # return redirect(request.META.get('HTTP_REFERER'))


@login_required
def edit(request, batch_pk):
    pass

    # batch = Batch.objects.get(id=batch_pk)
    # old_recieved_quantity = batch.received_quantity
    # form = BatchEditForm(request.POST or None, instance=batch)
    # if request.method == "POST":
    #     updated_batch = form.save()

    #     if old_recieved_quantity != updated_batch.received_quantity and updated_batch.received_quantity != 0:
    #         updateTotalInventory(batch.stock, batch)
    #         updated_batch.current_quantity = updated_batch.received_quantity
    #         updated_batch.save()
    #     return HttpResponseRedirect(reverse('asset:stock:batch:item:list', args=[batch_pk]))
    # context = {
    #     'batch': batch,
    #     'form': form,
    #     'form_url': reverse_lazy('asset:stock:batch:edit', args=[batch_pk]),
    # }
    # return render(request, "asset/stock/edit_batch.html", context)

def updateTotalInventory(stock, batch):
    pass

    # if batch.status == Batch.INSTOCK or batch.status == Batch.INTRANSIT:
    #     inventory, created = Total_Inventory.objects.update_or_create(
    #         asset=batch.asset, location=stock.location,
    #         defaults={"asset": batch.asset, "location": stock.location}
    #     )
    #     inventory.current_stock = inventory.current_stock + batch.received_quantity
    #     inventory.save()
