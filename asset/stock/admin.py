from django.contrib import admin
from .models import *

# from consumable.stock.models import Batch, Stock
# admin.site.register(Batch)

# admin.site.register(ItemAttribute)
# admin.site.register(Batch)
admin.site.register(Stock)
admin.site.register(StockItem)
