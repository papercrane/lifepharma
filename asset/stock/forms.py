from django import forms
from django.forms import ModelForm
from .models import Stock, StockCodeSetting as CodeSetting, StockItem as Item



class StockForm(ModelForm):
    class Meta:
        model = Stock

        widgets = {

            "code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "date_time": forms.TextInput(attrs={'class': "form-control tdate", "type": "text", 'required': "required"}),
            "location": forms.Select(attrs={'id': "location", 'class': 'form-control', "multiple": False}),
            "purchase_order": forms.Select(attrs={'id': "purchase_order", 'class': 'form-control', "multiple": False}),
            "purchased_by": forms.Select(attrs={'id': "purchased_by", 'class': 'form-control', "multiple": False}),

        }
        fields = ['code', 'date_time', 'location','purchase_order', 'purchased_by']

    def __init__(self, *args, **kwargs):
        super(StockForm, self).__init__(*args, **kwargs)
    # if 'enquiry' in self.fields:
    #     self.fields['enquiry'].queryset = Enquiry.objects.filter(status="open")


# class BatchForm(ModelForm):
#     class Meta:
#         model = Batch

#         widgets = {

#             "code": forms.TextInput(attrs={'class': "form-control"}),
#             "received_quantity": forms.NumberInput(attrs={'class': "form-control", 'label': "Received Quantity"}),
#             "date_of_manufacturing": forms.TextInput(attrs={'class': "form-control tdate", "type": "text"}),
#             "expiry_date": forms.TextInput(attrs={'class': "form-control tdate", "type": "text"}),
#             "ordered_quantity": forms.NumberInput(attrs={'class': "form-control", 'label': "Ordered Quantity", 'required': "required"}),
#             "price_per_item": forms.NumberInput(attrs={'class': "form-control", }),
#             "damage_quantity": forms.NumberInput(attrs={'class': "form-control", }),
#             "status": forms.Select(attrs={'id': "select1", 'class': 'form-control', "multiple": False}),
#             "asset": forms.Select(attrs={'class': 'form-control product', "multiple": False}),


#         }
#         fields = ['code', 'received_quantity', 'date_of_manufacturing', 'expiry_date', 'ordered_quantity',
#                   'price_per_item', 'damage_quantity', 'status', 'asset']
#         help_texts = {
#             'received_quantity': 'Exclude damaged Quantity',
#         }

#     def clean(self):
#         cleaned_data = super(BatchForm, self).clean()
#         status = cleaned_data.get("status")
#         received_quantity = cleaned_data.get("received_quantity")
#         ordered_quantity = cleaned_data.get("ordered_quantity")

#         if (status == Batch.INSTOCK or status == Batch.INTRANSIT) and received_quantity == 0:
#             # Only do something if both fields are valid so far.
#             self.add_error('received_quantity', "Received Quantity cannot be zero when status is Instock/Intransit")

#         elif status == Batch.PENDING and received_quantity > 0:
#             self.add_error('received_quantity', "Received Quantity cannot be zero when status is Pending")

#         if ordered_quantity < received_quantity:
#             self.add_error('received_quantity', "cannot be greater than Ordered Quantity")


class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


class ItemForm(ModelForm):
    class Meta:
        model = Item

        widgets = {

            # "code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
            # "shelf_id": forms.TextInput(attrs={'class': "form-control", 'id': "shelf_id"}),
            # "row_id": forms.TextInput(attrs={'class': "form-control", 'id': "row_id"}),
            # "column_id": forms.TextInput(attrs={'class': "form-control", 'id': "column_id"}),
            "status": forms.Select(attrs={'id': "status", 'class': 'form-control', "multiple": False}),

            "name": forms.TextInput(attrs={'id': "name", 'class': 'form-control', "multiple": False}),
            "state": forms.Select(attrs={'class': 'form-control', "multiple": False}),
            # "asset_type": forms.Select(attrs={'id': "asset_type", 'class': 'form-control', "multiple": False}),
            # "vendor": forms.Select(attrs={'id': "vendor", 'class': 'form-control', "multiple": False}),
            # "available_date": forms.DateInput(attrs={'class': "form-control tdate"}),
            "asset": forms.Select(attrs={'class': 'form-control product','required': "required", "multiple": False}),
            "expiry_date": forms.TextInput(attrs={'class': "form-control tdate", "type": "text",'required': "required"}),
            "price": forms.NumberInput(attrs={'class': "form-control1 price", 'id': "price"}),


        }
        fields = ['status', 'state','asset', 'expiry_date', 'price','name']


# class BatchEditForm(ModelForm):
#     class Meta:
#         model = Batch

#         widgets = {

#             "received_quantity": forms.NumberInput(attrs={'class': "form-control", 'label': "Received Quantity"}),
#             "date_of_manufacturing": forms.TextInput(attrs={'class': "form-control tdate", "type": "text"}),
#             "expiry_date": forms.TextInput(attrs={'class': "form-control tdate", "type": "text"}),
#             "ordered_quantity": forms.NumberInput(attrs={'class': "form-control", 'label': "Ordered Quantity"}),
#             "price_per_item": forms.NumberInput(attrs={'class': "form-control", }),
#             "msp": forms.NumberInput(attrs={'class': "form-control", }),
#             "damage_quantity": forms.NumberInput(attrs={'class': "form-control", }),


#         }
#         fields = ['received_quantity', 'date_of_manufacturing', 'expiry_date', 'ordered_quantity',
#                   'price_per_item', 'msp', 'damage_quantity', ]
#         help_texts = {
#             'received_quantity': 'Exclude damaged Quantity',
#         }


class ItemEditForm(ModelForm):
    class Meta:

        model = Item

        widgets = {
                "status": forms.Select(attrs={'id': "status", 'class': 'form-control', "multiple": False}),
                "state": forms.Select(attrs={'class': 'form-control', "multiple": False}),
                # "asset_type": forms.Select(attrs={'id': "asset_type", 'class': 'form-control', "multiple": False}),
                # "vendor": forms.Select(attrs={'id': "vendor", 'class': 'form-control', "multiple": False}),
                # "available_date": forms.DateInput(attrs={'class': "form-control tdate"}),
                "asset": forms.Select(attrs={'class': 'form-control product','required': "required", "multiple": False}),
                "expiry_date": forms.TextInput(attrs={'class': "form-control tdate", "type": "text",'required': "required"}),
                "price": forms.NumberInput(attrs={'class': "form-control1 price", 'id': "price"}),
                "name": forms.TextInput(attrs={'class': "form-control1"}),

        }
        fields = ['state','asset', 'expiry_date', 'status', 'price','name']


# class ItemAttributeForm(ModelForm):

#     class Meta:
#         model = ItemAttribute
#         widgets = {
#             "length": forms.NumberInput(attrs={'class': "form-control"}),
#             "breadth": forms.NumberInput(attrs={'class': "form-control"}),
#             "unit_of_mass": forms.Select(attrs={'class': 'form-control'}),
#             "weight": forms.NumberInput(attrs={'class': "form-control"}),
#             "unit_of_length": forms.Select(attrs={'class': 'form-control'}), "unit_of_weight": forms.Select(attrs={'class': 'form-control'}),
#             "volume": forms.NumberInput(attrs={'class': "form-control"}),
#             "unit_of_volume": forms.Select(attrs={'class': 'form-control'}),
#             "area": forms.NumberInput(attrs={'class': "form-control"}),
#             "unit_of_area": forms.Select(attrs={'class': 'form-control'}),
#             "size": forms.TextInput(attrs={'class': "form-control"}),
#             "manufacturing_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "warranty_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "last_repair_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "last_maintenance_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "brand": forms.TextInput(attrs={'class': "form-control"}),
#             "purchase_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "commissioning_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "asset_category": forms.Select(attrs={'class': 'form-control', "multiple": False}),
#             "current_value": forms.NumberInput(attrs={'class': "form-control"}),
#             "repair_cost": forms.NumberInput(attrs={'class': "form-control"}),
#             "sale_price": forms.NumberInput(attrs={'class': "form-control"}),
#             "registration_date": forms.DateInput(attrs={'class': "form-control tdate"}),
#             "registration_no": forms.NumberInput(attrs={'class': "form-control"}),

#         }
#         exclude = ['item']
