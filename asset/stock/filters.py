import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from asset.stock.models import Stock, StockItem as Item
from asset.models import AssetType, Asset



class StockFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))
    status = django_filters.ChoiceFilter(choices=Stock.CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

    order_by_created_at = django_filters.ChoiceFilter(
        label="Created date",
        name="created_at",
        method='order_by_field',
        choices=(('created_at', 'Ascending'), ('-created_at', 'Descending'),),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Stock
        fields = ['code', 'status']

    def filter_by_contact(self, queryset, name, value):
        return queryset.filter(
            Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
        )

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)


# class BatchFilter(django_filters.FilterSet):

#     code = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))
#     status = django_filters.ChoiceFilter(choices=Batch.CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
#     created_at = django_filters.DateTimeFromToRangeFilter(
#         name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

#     order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
#         ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

#     class Meta:
#         model = Batch
#         fields = ['code', 'status', 'created_at']

#     def filter_by_contact(self, queryset, name, value):
#         return queryset.filter(
#             Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
#         )

#     def order_by_field(self, queryset, name, value):
#         return queryset.order_by(value)

#     def my_custom_filter(self, queryset, name, value):
#         return queryset.filter(**{
#             name: value,
#         })


class ItemFilter(django_filters.FilterSet):

    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    status = django_filters.ChoiceFilter(choices=Item.CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
    created_at = django_filters.DateTimeFromToRangeFilter(
        name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
        ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Item
        fields = ['name', 'status', 'created_at']

    def filter_by_contact(self, queryset, name, value):
        return queryset.filter(
            Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
        )

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)

    def my_custom_filter(self, queryset, name, value):
        return queryset.filter(**{
            name: value,
        })


class AssetItemsFilter(django_filters.FilterSet):

    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    status = django_filters.ChoiceFilter(choices=Item.CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
    state = django_filters.ChoiceFilter(choices=Item.STATE, widget=forms.Select(attrs={'class': 'form-control'}))
    asset_type = django_filters.ModelChoiceFilter(
        queryset=AssetType.objects.all(),
        # method='filter_by_contact',
        name='asset_type__name',
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )
    asset = django_filters.ModelChoiceFilter(
        queryset=Asset.objects.all(),
        # method='filter_by_contact',
        name='asset',
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )
    # vendor = django_filters.ModelChoiceFilter(
    #     queryset=Vendor.objects.all(),
    #     # method='filter_by_contact',
    #     name='vendor',
    #     widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    # )



    # purchase_date = django_filters.DateFilter(label="Purchase Date",
    #                                           method="filterbyPaidDate", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))

    # commissioning_date = django_filters.DateFilter(label="Start to use Date",
    #                                                method="filterbyCommissioningDate", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))

    # last_maintenance_date = django_filters.DateFilter(label="Last Maintenance date",
    #                                                   method="filterbyLastMaintenanceDate", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))
    # shop_name = django_filters.CharFilter(lookup_expr='iexact', label="Shop Name",
    #                                       method='filter_by_shopname', widget=forms.TextInput(attrs={'class': 'form-control'}))
    # purchased_by = django_filters.CharFilter(lookup_expr='iexact', label="Purchased By",
    #                                          method='filter_by_purchased_by', widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Item
        fields = ['name', 'status', 'state', 'asset_type', 'asset']

    def filter_by_contact(self, queryset, name, value):
        return queryset.filter(
            Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
        )

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)

    def my_custom_filter(self, queryset, name, value):
        return queryset.filter(**{
            name: value,
        })

    def filterbyPaidDate(self, queryset, name, value):
        return queryset.filter(
            Q(attribute__purchase_date=value)
        )

    def filterbyLastMaintenanceDate(self, queryset, name, value):
        return queryset.filter(
            Q(attribute__last_maintenance_date=value)
        )

    def filterbyCommissioningDate(self, queryset, name, value):
        return queryset.filter(
            Q(attribute__commissioning_date=value)
        )

    def filter_by_shopname(self, queryset, name, value):
        return queryset.filter(
            Q(batch__stock__supplier__name__icontains=value) | Q(batch__stock__purchase_order__supplier__supplier__name__icontains=value)
        )

    def filter_by_purchased_by(self, queryset, name, value):
        return queryset.filter(
            Q(batch__stock__purchased_by__user__first_name__icontains=value)
        )
