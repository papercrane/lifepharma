from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render, redirect
from erp_core.pagination import paginate
from .filters import AssetItemsFilter
from asset.stock.models import StockItem as Item
from asset.models import Asset as Product, AssetType


def create(request, batch_pk):
    pass
    
    # batch = get_object_or_404(Batch, id=batch_pk)
    # assetform = ItemForm(request.POST or None)
    # attributeform = ItemAttributeForm(request.POST or None)
    # # import pdb; pdb.set_trace()
    # if request.method == "POST":

    #     if assetform.is_valid() and attributeform.is_valid():
    #         attribute = attributeform.save()
    #         asset = assetform.save(commit=False)
    #         asset.attribute = attribute
    #         asset.batch = batch
    #         asset.save()
    #         return redirect(reverse('asset:stock:batch:item:item_detail', args=[batch.pk, asset.pk]))

    # context = {
    #     'batch_pk': batch_pk,
    #     'assetform': assetform,
    #     'attributeform': attributeform,
    #     'form_url': reverse_lazy('asset:stock:batch:item:create', args=[batch.pk]),
    #     'type': "add"

    # }

    # return render(request, 'asset/stock/add_items.html', context)


def items(request, batch_pk):
    pass

    # batch = get_object_or_404(Batch, id=batch_pk)
    # left_items = batch.received_quantity - Item.objects.filter(batch=batch).count()

    # f = AssetItemsFilter(request.GET, queryset=Item.objects.filter(batch=batch).order_by("-created_at"))
    # items = f.qs
    # context = {
    #     'batch': batch,
    #     'items': items,
    #     'left_items': left_items,
    #     'filter': f,
    # }
    # return render(request, "asset/stock/items_detail.html", context)


def edit_status(request, batch_pk, item_pk):
    pass
    # new_status = request.POST['status']
    # # manual change of status to
    # # "sold","blocked","booked","BLOCKING_REQUEST","BOOKED" is not possible in
    # # quotation or invoice is there
    # if not isOnlyInventory() and (new_status == "sold" or new_status == "booked"):
    #     messages.add_message(request, messages.ERROR, "You cann't change the item status manually to " + new_status)
    #     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    # item = Item.objects.get(id=item_pk)
    # prev_status = item.status

    # item.status = new_status
    # item.save()

    # if item.status == prev_status:
    #     pass
    # else:
    #     pass
    #     # if item.status == "sold":
    #     #     #  Batch Current quantity is change by 1
    #     #     item.batch.current_quantity = item.batch.current_quantity - 1
    #     #     item.batch.save()

    #     #     if item.batch.current_quantity == 0:
    #     #         item.batch.status = Batch.SOLD
    #     #         total_inventory = Total_Inventory.objects.get(
    #     #             product=item.batch.product, location=item.batch.stock.location)
    #     #         total_inventory.current_stock = total_inventory.current_stock - item.batch.current_quantity
    #     #         total_inventory.save()
    #     #         item.batch.stock.current_batches = item.batch.stock.current_batches - 1
    #     #         if item.batch.stock.current_batches == 0:
    #     #             item.batch.stock.status = Stock.SOLD
    #     #             item.batch.stock.save()
    #     #     item.save()

    # return HttpResponseRedirect(reverse('asset:stock:batch:item:list', args=[item.batch.id]))


def delete(request, stock_pk, item_pk):

    item = Item.objects.get(pk=item_pk)
    # if item.status == "sold":
    # item.batch.current_quantity = item.batch.current_quantity + 1
    # item.batch.save()
    item.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def edit(request, batch_pk, item_pk):
    pass

    # asset = get_object_or_404(Item, pk=item_pk)
    # assetform = ItemForm(request.POST or None, instance=asset)
    # attributeform = ItemAttributeForm(request.POST or None, instance=asset.attribute)

    # prev_status = asset.status

    # if request.method == "POST":

    #     if assetform.is_valid() and attributeform.is_valid():
    #         attribute = attributeform.save()
    #         asset = assetform.save(commit=False)
    #         asset.attribute = attribute

    #         # # asset status changed in to sold
    #         # if asset.status == prev_status:
    #         #     pass
    #         # else:
    #         #     if asset.status == Item.SOLD:
    #         #         print("sold......")
    #         #         #  Batch Current quantity is change by 1
    #         #         asset.batch.current_quantity = asset.batch.current_quantity - 1
    #         #         asset.batch.save()

    #         #         if asset.batch.current_quantity == 0:
    #         #             asset.batch.status = Batch.SOLD
    #         #             total_inventory = Total_Inventory.objects.get(
    #         #                 asset=asset.batch.asset, location=asset.batch.stock.location)
    #         #             total_inventory.current_stock = total_inventory.current_stock - asset.batch.current_quantity
    #         #             total_inventory.save()
    #         #             asset.batch.stock.current_batches = asset.batch.stock.current_batches - 1
    #         #             if asset.batch.stock.current_batches == 0:
    #         #                 asset.batch.stock.status = Stock.SOLD
    #         #                 asset.batch.stock.save()
    #         #         asset.save()

    #         asset.save()
    #         return redirect(reverse('asset:stock:batch:item:item_detail', args=[batch_pk, asset.pk]))

    # context = {
    #     'assetform': assetform,
    #     'batch_pk': batch_pk,
    #     'attributeform': attributeform,
    #     'form_url': reverse_lazy('asset:stock:batch:item:edit', args=[batch_pk, item_pk]),
    #     'type': "edit"

    # }

    # return render(request, 'asset/stock/add_items.html', context)


@login_required
def edit(request, stock_pk, item_pk):
    item = Item.objects.get(id=item_pk)
    # print("item.....", ItemEditForm())
    form = ItemEditForm(request.POST or None, instance=item)
    if request.method == "POST":
        updated_item = form.save()
        updated_item.save()
        # return HttpResponseRedirect(reverse('asset:stock:stock_items', args=[batch.pk]))
        return redirect(reverse('asset:stock:stock_detail', args=[stock_pk]))
    context = {
        'item': item,
        'form': form,
        'form_url': reverse_lazy('asset:stock:item:edit', args=[item.stock.pk, item_pk]),
    }
    return render(request, "asset/stock/edit_item.html", context)


@login_required
def item_detail(request, stock_pk, item_pk):
    # pass

    item = get_object_or_404(Item, pk=item_pk)
    context = {
        'item': item,
    }

    return render(request, "asset/stock/single_item_detail.html", context)


@login_required
def assetLists(request):
    # pass

    f = AssetItemsFilter(request.GET,queryset=Item.objects.all().exclude(asset__staff_asset=True))
    # vendor_id = request.GET.get('vendor')
    asset_type_id = request.GET.get('asset_type')
    asset_id = request.GET.get('asset')

    asset_name = ""
    asset_type= ""
    if asset_id:
        asset_name = Product.objects.get(id=asset_id)
    if asset_type_id:
        asset_type = AssetType.objects.get(id=asset_type_id)

    assets, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)

    # assets, pagination = paginate(request, queryset=assets, per_page=20, default=True)

    context = {
        'assets': assets,
        'pagination': pagination,
        'filter': f,
        'asset_name': asset_name,
        'asset_type': asset_type,
    }

    return render(request, "asset/stock/assets_list.html", context)

@login_required
def staff_asset_lists(request):
    pass

    # f = AssetItemsFilter(request.GET,queryset=Item.objects.filter(batch__asset__staff_asset=True))
    # vendor_id = request.GET.get('vendor')
    # asset_type_id = request.GET.get('asset_type')

    # vendor_name = ""
    # asset_type= ""
    # if vendor_id:
    #     vendor_name = Vendor.objects.get(id=vendor_id)
    # if asset_type_id:
    #     asset_type = AssetType.objects.get(id=asset_type_id)

    # assets, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)

    # # assets, pagination = paginate(request, queryset=assets, per_page=20, default=True)

    # context = {
    #     'assets': assets,
    #     'pagination': pagination,
    #     'filter': f,
    #     'vendor_name': vendor_name,
    #     'asset_type': asset_type,
    # }

    # return render(request, "asset/stock/staff_asset_list.html", context)
