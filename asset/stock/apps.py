import os
from django.apps import AppConfig


class StockConfig(AppConfig):
    name = 'asset.stock'
    label = "asset_stock"
    path = os.path.dirname(os.path.abspath(__file__))
