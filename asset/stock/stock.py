from django.core.urlresolvers import reverse, reverse_lazy
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.forms import inlineformset_factory, modelform_factory

from django.shortcuts import get_object_or_404, render, redirect
from erp_core.pagination import paginate
from asset.purchase_order.models import PurchaseOrder, PurchaseOrderItem

from .filters import StockFilter, ItemFilter, AssetItemsFilter
from .models import Stock, StockCodeSetting

from .forms import *
from asset.stock.models import StockItem as Item

def getStockForms(request, po_pk=None):

    # if po_pk:
    #     asset_po = get_object_or_404(PurchaseOrder, id=po_pk)
    #     PurchaseOrderItem = list(asset_po.order_items.values('asset', 'price'))
    #     plist = []
    #     for item in PurchaseOrderItem:
    #         plist.append({
    #             # 'ordered_quantity': item['ordered_quantity'],
    #             'asset': item['asset'],
    #             'price': item['price'],
    #         })

    #     c = len(PurchaseOrderItem) - 1
    #     print(plist)
    #     ItemInlineFormSet = inlineformset_factory(
    #         Stock, Item, form=ItemForm, extra=c, can_delete=True, min_num=1, validate_min=True)

    #     items_formset = ItemInlineFormSet(request.POST or None, initial=plist)

    #     POIncludedStockForm = modelform_factory(Stock, form=StockForm)
    #     stock_instance = None

    #     stock_form = POIncludedStockForm(request.POST or None,initial={
    #                                      'purchase_order': asset_po})
    # else:
    ItemInlineFormSet = inlineformset_factory(
        Stock, Item, form=ItemForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    items_formset = ItemInlineFormSet(request.POST or None)
    stock_form = StockForm(request.POST or None)

    return (stock_form, items_formset)


def updateTotalInventory(stock, batch):

    # if batch.status == Batch.INSTOCK or batch.status == Batch.INTRANSIT:
    #     inventory, created = Total_Inventory.objects.update_or_create(
    #         asset=batch.asset, location=stock.location,
    #         defaults={"asset": batch.asset, "location": stock.location}
    #     )
    #     inventory.current_stock = inventory.current_stock + batch.received_quantity
    #     inventory.save()
    pass


@transaction.atomic
def create(request, po_pk=None):

    po_pk = request.GET.get('po', False) or request.POST.get('purchase_order', False)
    stock_form, items_formset = getStockForms(request, po_pk)
    if request.method == "POST":
        print(stock_form.errors, items_formset.errors)
        if stock_form.is_valid() and items_formset.is_valid():
            stock = stock_form.save()
            items = items_formset.save(commit=False)

            for item in items:
                # batch.current_quantity = batch.received_quantity
                item.stock = stock
                item.save()
                item.generate_qrcode()
                # Now only setting the stock status to instock atleast one batch is in stock
                # if batch.status == Batch.INSTOCK:
                #     stock.status = Stock.INSTOCK
                # if batch.status == Batch.INSTOCK or batch.status == Batch.INTRANSIT:
                #     stock.current_batches = stock.current_batches + 1
                # batch.save()
                # if batch.received_quantity != 0:
                #     updateTotalInventory(stock, batch)
            stock.save()

            # Update the field of inventory purchaseorder items
            # if po_pk:
            #     poitems = PurchaseOrderItem.objects.filter(purchase_order=stock.purchase_order)

            #     for i in range(len(poitems)):
            #         if poitems[i].product.pk == batches[i].product.pk:
            #             poitems[i].damaged_quantity = poitems[i].damaged_quantity + batches[i].damage_quantity
            #             poitems[i].delivered_quantity = poitems[i].delivered_quantity + batches[i].received_quantity
            #             poitems[i].save()
            return redirect(reverse('asset:stock:stock_detail', args=[stock.id]))
    context = {
        "stock_form": stock_form,
        'items_formset': items_formset,
        'form_url': reverse_lazy('asset:stock:create'),
        "type": "create"
    }

    return render(request, 'asset/stock/edit.html', context)


def index(request):
    f = StockFilter(request.GET, queryset=Stock.objects.all().order_by("-created_at"))
    stocks, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)

    context = {
        'stocks': stocks,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "asset/stock/list.html", context)


@transaction.atomic
def code_settings(request):

    if StockCodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=StockCodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            return redirect(reverse('asset:stock:list'))

    context = {
        'code_form': code_form,
    }

    return render(request, "asset/stock/code_setting.html", context)


def detail(request, stock_pk):

    stock = get_object_or_404(Stock, id=stock_pk)
    items = Item.objects.filter(stock=stock)
    f = AssetItemsFilter(request.GET, queryset=Item.objects.filter(stock=stock).order_by("-created_at"))
    # # print("Batch f.batches : ", f.qs)
    items = f.qs

    item_statuses = Item.CHOICES
    stock_statuses = Stock.CHOICES

    context = {
        'stock': stock,
        'items': items,
        'filter': f,
        'item_statuses': item_statuses,
        'stock_statuses': stock_statuses
    }
    return render(request, "asset/stock/detail.html", context)


@transaction.atomic
def edit(request, stock_pk):
    # pass

    stock = Stock.objects.get(id=stock_pk)
    # batchs = stock.asset_batches.all()
    stock_form = StockForm(request.POST or None, instance=stock)
    if stock.purchase_order:
        asset_po = get_object_or_404(PurchaseOrder, id=stock.purchase_order_id)

        purchase_order_items = list(asset_po.order_items.values('asset', 'price'))
        plist = []
        for item in purchase_order_items:
            plist.append({
                'asset': item['asset'],
                'price': item['price'],
            })

        c = len(purchase_order_items) - 1

        ItemInlineFormSet = inlineformset_factory(
            Stock, Item, form=ItemForm, extra=c, can_delete=True, min_num=1, validate_min=True)

        items_formset = ItemInlineFormSet(request.POST or None, initial=plist)
    else:
        ItemInlineFormSet = inlineformset_factory(
            Stock, Item, form=ItemForm, extra=0, can_delete=True, min_num=1, validate_min=True)

        items_formset = ItemInlineFormSet(request.POST or None)

    if request.method == "POST":
        # print(stock_form.errors, batches_formset.errors)
        if stock_form.is_valid() and items_formset.is_valid():
            stock_form.save()

            # kalabha edit starts
            items = items_formset.save(commit=False)

            for item in items:
                # batch.current_quantity = batch.received_quantity
                item.stock = stock
                item.save()
                # # Now only setting the stock status to instock atleast one batch is in stock
                # if batch.status == Batch.INSTOCK:
                #     stock.status = Stock.INSTOCK
                # if batch.status == Batch.INSTOCK or batch.status == Batch.INTRANSIT:
                #     stock.current_batches = stock.current_batches + 1
                # batch.save()
                # if batch.received_quantity != 0:
                #     updateTotalInventory(stock, batch)
            stock.save()

            # if isInventoryPO():
            #     poitems = PurchaseOrderItem.objects.filter(purchase_order=stock.purchase_order)
            #     for i in range(len(poitems)):
            #         if poitems[i].asset.pk == batches[i].asset.pk and batches[i].asset.pk == batchs[i].asset.pk:
            #             dmg_qty = batches[i].damage_quantity - batchs[i].damage_quantity
            #             poitems[i].damaged_quantity = poitems[i].damaged_quantity + dmg_qty
            #             del_qty = batches[i].received_quantity - batchs[i].received_quantity
            #             poitems[i].delivered_quantity = poitems[i].delivered_quantity + del_qty
            #             poitems[i].save()
            return HttpResponseRedirect(reverse('asset:stock:stock_detail', args=[stock.id]))

    context = {
        'stock_form': stock_form,
        'items_formset': items_formset,
        'form_url': reverse_lazy('asset:stock:stock_edit', args=[stock.pk]),
        'type': "edit",
        'stock': stock,
    }
    return render(request, "asset/stock/edit.html", context)


def checkStock(request):
    pass
    # products = Product.fixed_asset.all()
    # if request.method == "POST":
    #     current_product = request.POST.get('product')
    #     if current_product:
    #         batches = Batch.objects.filter(asset_id=current_product)
    #         count_instock = batches.filter(status=Batch.INSTOCK).count()
    #         count_intransit = batches.filter(status=Batch.INTRANSIT).count()
    #         count_booked = batches.filter(status=Batch.BOOKED).count()

    #     stocks = PurchaseOrderItem.objects.filter(asset_id=current_product, expected_date__gte=timezone.now(
    #     ).date()).values("expected_date", "asset__name", "ordered_quantity")
    #     context = {
    #         'products': products,
    #         'count_instock': count_instock,
    #         'count_booked': count_booked,
    #         'count_intransit': count_intransit,
    #         'stocks': stocks,
    #         'current_product': current_product,
    #     }
    # else:
    #     context = {
    #         'products': products,
    #     }

    # return render(request, "asset/stock/checkStock.html", context)


@transaction.atomic
def edit_status(request, stock_pk):
    # pass

    new_status = request.POST['status']

    # get the batch and change its status
    stock = Stock.objects.get(id=stock_pk)

    # batches_left = Batch.objects.filter(stock=stock).exclude(status=Batch.SOLD).count()

    # # manual change of status to "sold",is not possible in quotation or invoice is there
    # if new_status == Stock.SOLD and batches_left != 0:
    #     messages.add_message(request, messages.ERROR, "You can't change status to Sold. " +
    #                          str(batches_left) + " more batches is left in stock")
    #     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    # prev_status = stock.status
    stock.status = new_status
    stock.save()
    return redirect(reverse('asset:stock:stock_detail', args=[stock.id]))
