from django.db import models
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting
from asset.models import Asset as Product, AssetType
from location.models import Location
from asset.purchase_order.models import PurchaseOrder
import qrcode
from papercraneerp.settings import DOMAIN
import io

from django.core.files.uploadedfile import InMemoryUploadedFile
# from insurance.models import InsuranceDetail


# class ManufactureShift(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255, unique=True)

#     class Meta:
#         app_label = 'asset'

#     def __str__(self):
#         return str(self.name)


# class Manufacture(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255)
#     code = models.CharField(max_length=30, unique=True)

#     class Meta:
#         app_label = 'asset'

#     def __str__(self):
#         return self.name + '(' + str(self.code) + ')'


# class Batch(ErpAbstractBaseModel):
#     PENDING = 'Pending'
#     INTRANSIT = 'Intransit'
#     INSTOCK = 'Instock'
#     SOLD = 'Sold'

#     # BLOCKED = 'Blocked'
#     # BLOCKING_REQUEST = 'Request for block'
#     CHOICES = (
#         (PENDING, 'Pending'),
#         (INTRANSIT, 'Intransit'),
#         (INSTOCK, 'Instock'),
#         (SOLD, 'Sold'),
#     )
#     code = models.CharField(max_length=30, unique=True)
#     received_quantity = models.IntegerField("Recieved Quantity", null=True, blank=True, default=0)
#     date_of_manufacturing = models.DateField(null=True, blank=True)
#     expiry_date = models.DateField(null=True, blank=True)
#     current_quantity = models.IntegerField(null=True, blank=True, default=0)
#     price_per_item = models.DecimalField(max_digits=20, decimal_places=2)
#     msp = models.DecimalField("Minimum Selling Price", max_digits=20, decimal_places=2, null=True, blank=True)
#     damage_quantity = models.IntegerField(null=True, blank=True, default=0)
#     status = models.CharField(max_length=20, choices=CHOICES, default=PENDING)
#     ordered_quantity = models.IntegerField(null=True, blank=True, default=0)

#     manufacturer = models.ForeignKey(Manufacture, null=True, blank=True)
#     shift = models.ForeignKey(ManufactureShift, on_delete=models.CASCADE,
#                               null=True, blank=True, related_name="%(class)ss")
#     asset = models.ForeignKey(Product, related_name='asset_batches')
#     stock = models.ForeignKey("Stock", related_name="asset_batches")
#     # booked_by = models.ForeignKey(
#     #     'employee.Employee', on_delete=models.CASCADE, null=True, blank=True, related_name="booked_asset_batches")
#     # lot = models.ForeignKey(Lot, default=uuid.uuid4)

#     class Meta:
#         app_label = 'asset'

#     def __str__(self):
#         return str(self.code)

#     def booked(self, employee):
#         # self.status = self.BOOKED
#         # self.booked_by = employee
#         return self

#     def cancelbooked(self):
#         # self.status = self.INSTOCK
#         return self

#     def Isitems(self):

#         return self.asset_stock_item_set.filter(state=StockItem.AVAILABLE).count() > 0 and self.status == Batch.INSTOCK


# include supplier, purchase order, location


class Stock(ErpAbstractBaseModel):
    PENDING = 'Pending'
    INTRANSIT = 'Intransit'
    INSTOCK = 'Instock'
    SOLD = 'Sold'
    CHOICES = (
        (PENDING, 'Pending'),
        (INTRANSIT, 'Intransit'),
        (INSTOCK, 'Instock'),
        (SOLD, 'Sold'),
    )

    def newCode():
        return StockCodeSetting.new_code()

    code = models.CharField(max_length=30, unique=True, default=newCode)
    date_time = models.DateTimeField()

    location = models.ForeignKey(Location, related_name="asset_stock_location")
    # supplier = models.ForeignKey(Supplier, null=True, blank=True, related_name="asset_stock_supplier")
    purchase_order = models.ForeignKey(PurchaseOrder, null=True, blank=True, related_name="asset_stock_purchase_order")
    status = models.CharField(max_length=10, choices=CHOICES, default=PENDING)
    # current_batches = models.PositiveIntegerField(default=0)
    created_by = models.ForeignKey('employee.Employee', on_delete=models.CASCADE,
                                   null=True, blank=True, related_name="asset_stock_created_by")
    purchased_by = models.ForeignKey('employee.Employee', on_delete=models.CASCADE,
                                     null=True, blank=True, related_name="stock_purchased_by")

    class Meta:
        app_label = 'asset'

    def __str__(self):
        return str(self.code)

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            StockCodeSetting.incrementCountIndex()

        return super(Stock, self).save(*args, **kwargs)

    # def hasPurchaseOrder(self):
    #     return GlobalConfig.hasModule('inventory_po')





class StockItem(ErpAbstractBaseModel):
    PENDING = 'Pending'
    INTRANSIT = 'Intransit'
    INSTOCK = 'Instock'
    SOLD = 'Sold'
    # BOOKED = 'Booked'
    # BLOCKED = 'Blocked'
    # BLOCKING_REQUEST = 'request for block'

    CHOICES = (
        (PENDING, 'Pending'),
        (INTRANSIT, 'Intransit'),
        (INSTOCK, 'Instock'),
        (SOLD, 'Sold'),
        # (BOOKED, 'Booked'),
        # (BLOCKED, 'Blocked'),
        # (BLOCKING_REQUEST, 'request for block'),
    )

    AVAILABLE = 'Available'
    DAMAGED = 'Damaged'
    MAINTANCEMODE = 'Mainatance Mode'
    ASSIGNED = 'Assigned'
    NOTAVAILABLE = 'Not Available'
    STATE = (
        (NOTAVAILABLE, 'Not Available'),
        (AVAILABLE, 'Available'),
        (DAMAGED, 'Damaged'),
        (MAINTANCEMODE, 'Mainatance Mode'),
        (ASSIGNED, 'Assigned')
        # (BOOKED, 'Booked'),
        # (BLOCKED, 'Blocked'),
        # (BLOCKING_REQUEST, 'request for block'),
    )

    state = models.CharField(max_length=30, choices=STATE, default=AVAILABLE,null=True,blank=True)
    # code = models.CharField(max_length=30, unique=True,null=True,blank=True)
    asset = models.ForeignKey(Product, related_name='asset_batches')
    expiry_date = models.DateField(null=True, blank=True)
    
    name = models.CharField(max_length=30,null=True, blank=True)
    # shelf_id = models.CharField(max_length=255, null=True, blank=True)
    # row_id = models.CharField(max_length=255, null=True, blank=True)
    # column_id = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=10, choices=CHOICES, default=INSTOCK,null=True,blank=True)
    stock = models.ForeignKey(Stock, related_name="asset_stock_item_set",null=True,blank=True)
    # attribute = models.OneToOneField('asset_stock.ItemAttribute', on_delete=models.CASCADE, null=True, blank=True)
    asset_type = models.ForeignKey(AssetType, related_name="asset_type", null=True, blank=True)
    # vendor = models.ForeignKey(Vendor, related_name="asset_vendor", null=True, blank=True)
    available_date = models.DateTimeField(null=True, blank=True)
    # insurances = GenericRelation(InsuranceDetail)
    price = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    qrcode = models.ImageField(upload_to='qrcode', blank=True, null=True)    


    class Meta:
        app_label = 'asset'

    def __str__(self):
        return str(self.name)

    def generate_qrcode(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=6,
            border=0,
        )

        stock_id = str(self.stock.pk)
        item_id = str(self.pk)
        qr.add_data(DOMAIN+stock_id+'/items/'+item_id+'/item_detail/')
        qr.make(fit=True)
        img = qr.make_image()
        buffer = io.BytesIO()
        img.save(buffer)
        filename = 'qr-code-%s.png' % (self.pk)
        filebuffer = InMemoryUploadedFile(buffer, None, filename, 'image/png', buffer.getbuffer().nbytes, None)
        self.qrcode.save(filename, filebuffer)
        

    # def jobDate(self):

    #     jobs = self.jobtools.all()
    #     for job in jobs:
    #         print(job.job_service.job.start_date_and_time)
    #         print(job.job_service.job.end_date_and_time)
    #     return jobs


class StockCodeSetting(BaseCodeSetting):

    class Meta:
        app_label = 'asset'


# class ItemAttribute(ErpAbstractBaseModel):

#     SERVICEPROVIDEPERITEM = 'Service Provide Per Item'
#     SERVICEPROVIDEPERCATEGORY = 'Service Provide Per Category'
#     ASSET_CATEGORY = (
#         (SERVICEPROVIDEPERITEM, 'Service Provide Per Item'),
#         (SERVICEPROVIDEPERCATEGORY, 'Service Provide Per Category'),
#     )

#     DISTANCE_UNITS = (
#         ('m', 'meter'),
#         ('in', 'inch'),
#         ('ft', 'foot'),
#         ('cm', 'centimeter'),
#         ('mm', 'millimeter'),
#     )
#     MASS_UNITS = (
#         ('kg', 'kilogram'),
#         ('mg', 'milligram'),
#         ('g', 'gram'),
#         ('cm', 'centimeter'),
#         ('mm', 'millimeter'),
#     )

#     VOLUME_UNITS = (
#         ('cc', 'cubic centimeter'),
#         ('L', 'liter'),
#     )
#     AREA_UNITS = (
#         ('m2', 'square meters'),
#         ('cm2', 'square centimeter'),
#         ('ft2', 'square feet'),
#     )

#     unit_of_area = models.CharField(max_length=30, choices=AREA_UNITS, default=AREA_UNITS[0], blank=True, null=True)
#     area = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)

#     unit_of_volume = models.CharField(max_length=30, choices=VOLUME_UNITS,
#                                       default=VOLUME_UNITS[0], blank=True, null=True)
#     volume = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)

#     unit_of_length = models.CharField(max_length=30, choices=DISTANCE_UNITS,
#                                       default=DISTANCE_UNITS[0], blank=True, null=True)
#     length = models.IntegerField(null=True, blank=True)
#     breadth = models.IntegerField(null=True, blank=True)

#     unit_of_mass = models.CharField(max_length=30, choices=MASS_UNITS, default=MASS_UNITS[0], blank=True, null=True)
#     weight = models.IntegerField(null=True, blank=True)

#     size = models.CharField(max_length=255, null=True, blank=True)
#     manufacturing_date = models.DateField(null=True, blank=True)
#     warranty_date = models.DateField(null=True, blank=True)
#     last_repair_date = models.DateField(null=True, blank=True)
#     last_maintenance_date = models.DateField('Next Maintenance Date', null=True, blank=True)
#     brand = models.CharField(max_length=255, null=True, blank=True)
#     purchase_date = models.DateField(null=True, blank=True)
#     commissioning_date = models.DateField(null=True, blank=True)
#     asset_category = models.CharField(max_length=30, choices=ASSET_CATEGORY, default=SERVICEPROVIDEPERITEM)
#     current_value = models.IntegerField(null=True, blank=True)
#     repair_cost = models.IntegerField(null=True, blank=True)
#     sale_price = models.IntegerField(null=True, blank=True)
#     registration_date = models.DateField(null=True, blank=True)
#     registration_no = models.IntegerField(null=True, blank=True)
