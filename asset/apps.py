from django.apps import AppConfig
# from .models import Asset


class AssetConfig(AppConfig):
    name = 'asset'
    label = 'asset'

    # def ready(self):
    #     names = ['Immovable', 'Movable', ]
    #
    #     from .models import AssetType, Asset
    #     for name in names:
    #         AssetType.objects.update_or_create(name=name, defaults={'name': name})
    #
    #     assets = ['Vehicle', 'Mobileconnection', 'Netsetterconnection', 'Websocialmedia']
    #     for asset in assets:
    #         Asset.objects.update_or_create(name=asset, code=asset, defaults={'name': asset, 'code': asset, 'asset_type': AssetType.objects.get(name='Immovable'), "own_asset": 1},)
