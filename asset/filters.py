import django_filters
from django_filters import widgets
from django import forms
from asset.models import Asset, AssetType
from employee.models import Employee


class AssetFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    asset_type = django_filters.ModelChoiceFilter(
        queryset=AssetType.objects.all(),
        # method='filter_by_city',
        name="asset_type",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", })
    )

    # created_at = django_filters.DateTimeFromToRangeFilter(name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))

    order_by_created_at = django_filters.ChoiceFilter(
        label="Created date",
        name="created_at",
        method='order_by_field',
        choices=(('created_at', 'Ascending'), ('-created_at', 'Descending'),),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Asset
        fields = ['code', 'name', 'created_at']

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)


