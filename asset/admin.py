from django.contrib import admin
from asset.models import Asset, AssetType, AssetTransaction, AssetCategory
admin.site.register(Asset)
admin.site.register(AssetType)
admin.site.register(AssetTransaction)
admin.site.register(AssetCategory)


