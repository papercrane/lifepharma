from django.db import models

from customer.models import Customer
from employee.models import Employee
from erp_core.models import ErpAbstractBaseModel



class AssetBaseModel(ErpAbstractBaseModel):

    code = models.CharField(max_length=255, null=True, blank=True,unique=True)
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)

    class Meta:
        abstract = True

    # def save(self, *args, **kwargs):
    #     if self.code and self.created_at is None:
    #         CodeSetting.incrementCountIndex()
    #
    #     return super(AssetBaseModel, self).save(*args, **kwargs)


class AssetType(ErpAbstractBaseModel):

    name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)


class AssetCategory(ErpAbstractBaseModel):

    name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)


# class DahlBookManager(models.Manager):
#     def get_queryset(self):
#         return super(DahlBookManager, self).get_queryset().filter(author='Roald Dahl')


class FixedAssetManager(models.Manager):
    def get_queryset(self):
        return super(FixedAssetManager, self).get_queryset().filter(customer_asset=False)


class Asset(AssetBaseModel):
    CHOICES = (
        ('sq.feet', 'sq.feet'),
        ('hours', 'hours'),
    )

    objects = models.Manager()
    asset_type = models.ForeignKey(AssetType)
    asset_category = models.ForeignKey(AssetCategory,null=True)
    manufacturing_date=models.DateField(null=True)
    expiry_date=models.DateField(null=True)
    returnable=models.BooleanField(default=True)
    warranty=models.BooleanField(default=True)
    warranty_duration=models.CharField(max_length=30,null=True,blank=True)
    used=models.BooleanField(default=False)
    available=models.BooleanField(default=True)


    def __str__(self):
        return str(self.name)


class AssetTransaction(ErpAbstractBaseModel):
    STATUS_DAMAGE = 'Damage'
    STATUS_DESTROYED = 'Destroyed'
    STATUS_FUNCTIONAL = 'Functional'
    STATUS_USED = 'Used'
    STATUS_NOTUSED = 'Not Used'

    ASSET_STATUS = (
        (STATUS_DAMAGE, "Damage"),
        (STATUS_DESTROYED, "Destroyed"),
        (STATUS_FUNCTIONAL, "Functional"),


    )

    USAGE_STATUS = (
        (STATUS_USED, 'Used'),
        (STATUS_NOTUSED, 'Not Used'),
    )

    asset=models.ForeignKey(Asset,related_name="asset")
    customer=models.ForeignKey(Customer,related_name="transactions")
    date_and_time=models.DateTimeField()
    source=models.ForeignKey(Employee,related_name="source")
    destination=models.ForeignKey(Employee,null=True,related_name="destination",blank=True)
    created_by=models.ForeignKey(Employee,related_name="created_by")
    updated_by=models.ForeignKey(Employee,related_name="updated_by")
    asset_status=models.CharField(max_length=15,choices=ASSET_STATUS,default=STATUS_FUNCTIONAL)
    usage_status=models.CharField(max_length=15,choices=USAGE_STATUS,default=STATUS_NOTUSED)
    parent = models.ForeignKey('self',null=True,related_name="predecessors",blank=True)

    def __str__(self):
        return str(self.asset)+str(self.date_and_time)



