from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Asset, AssetType, AssetTransaction


class AssetForm(ModelForm):
    class Meta:
        model = Asset

        fields = ['name','asset_category','asset_type','manufacturing_date','expiry_date','warranty_duration','warranty','returnable']

        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "asset_category": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "asset_type": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "manufacturing_date": forms.DateInput(attrs={'class': "form-control tdate", 'required': "required"}),
            "expiry_date": forms.DateInput(attrs={'class': "form-control tdate", 'required': "required"}),
            "warranty_duration": forms.TextInput(attrs={'class': "form-control"}),
            "warranty": forms.CheckboxInput(),
            "returnable": forms.CheckboxInput(),

        }


class CustomerAssetForm(ModelForm):
    class Meta:
        model = Asset

        exclude = ['code','location']

        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            # "code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "price": forms.NumberInput(attrs={'class': "form-control", }),
            "asset_type": forms.Select(attrs={'class': 'form-control', "multiple": False}),
            # "location": forms.Select(attrs={'id': "location", 'class': 'form-control', "multiple": False}),
            # "own_asset": forms.CheckboxInput(),
            # "rent_asset": forms.CheckboxInput(),
            # "customer_asset": forms.CheckboxInput(),
            "price_based_on": forms.Select(attrs={'class': 'form-control', "multiple": False}),
            # "staff_asset": forms.CheckboxInput(),

        }


class AssetTypeForm(ModelForm):
    class Meta:
        model = AssetType
        fields = ('name',)
        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control"}),
        }



class TransactionForm(ModelForm):
    class Meta:
        model = AssetTransaction
        fields = ('customer','date_and_time','source','destination','parent')
        widgets = {

            "customer": forms.Select(attrs={'class': "form-control",'required': "required"}),
            "source": forms.Select(attrs={'class': "form-control",'required': "required"}),
            "destination": forms.Select(attrs={'class': "form-control"}),
            "parent": forms.Select(attrs={'class': "form-control", }),
            "date_and_time": forms.DateInput(attrs={'class': "form-control tdate"}),

        }
