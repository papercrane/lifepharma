from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from asset.forms import AssetTypeForm
from django.shortcuts import get_object_or_404, render
from location.models import *
from django.contrib import messages
from erp_core.pagination import paginate
from django.http import HttpResponseRedirect

from asset.models import AssetType, AssetCategory


# from asset.filters import AssetFilter


# @login_required
# def index(request):
#     # print(request.GET)

#     f = AssetFilter(request.GET, queryset=Asset.objects.all().order_by("-created_at"))
#     # assets = Asset.objects.all()

#     assets, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
#     context = {
#         'assets': assets,
#         # 'status': Customer.STATUSES,
#         'pagination': pagination,

#         'filter': f,
#     }

#     return render(request, "asset/index.html", context)


# @login_required
# def create(request):
#     assettypeform = AssetTypeForm(request.POST or None)


#     if request.method == "POST":

#         if assettypeform.is_valid():
#             asset_type = assettypeform.save()

#             return HttpResponseRedirect(reverse('asset:create'))

#     context = {
#         'assettypeform': assettypeform,
#         'form_url': reverse_lazy('asset:asset_type:create'),
#         'type': "add",

#     }

#     return render(request, "asset_type/create.html", context)


# @login_required
# def edit(request, pk):
#     asset = get_object_or_404(Asset, pk=pk)
#     assetform = AssetForm(request.POST or None, instance=asset)
#     attributeform = AttributeForm(request.POST or None, instance=asset.attribute)

#     if request.method == "POST":

#         if assetform.is_valid() and attributeform.is_valid():
#             attribute = attributeform.save()
#             asset = assetform.save(commit=False)
#             asset.attribute = attribute
#             asset.save()
#             return HttpResponseRedirect(reverse('asset:list'))

#     context = {
#         'assetform': assetform,
#         'attributeform': attributeform,
#         'form_url': reverse_lazy('asset:edit', args=[asset.pk]),
#         'type': "edit",

#     }

#     return render(request, "asset/create.html", context)


# @login_required
# def detail(request, pk):

#     asset = get_object_or_404(Asset, pk=pk)

#     context = {
#         'asset': asset,
#     }

#     return render(request, "asset/detail.html", context)


# @login_required
# def delete(request, pk):
#     if request.method == 'POST':
#         asset = get_object_or_404(Asset, pk=pk)
#         asset.delete()
#         return HttpResponseRedirect(reverse('asset:list'))


@login_required
def create(request):

    if request.method == "POST":
        hid = request.POST.get('hid')
        asset_type = request.POST.get('asset-type')
        AssetType.objects.create(name=asset_type)
        if hid:
            return HttpResponseRedirect(reverse('asset:customer_asset_create'))
        else:
            return HttpResponseRedirect(reverse('asset:create'))


@login_required
def asset_category(request):

    if request.method == "POST":
        hid = request.POST.get('hid')
        asset_type = request.POST.get('asset-category')
        AssetCategory.objects.create(name=asset_type)
        if hid:
            return HttpResponseRedirect(reverse('asset:customer_asset_create'))
        else:
            return HttpResponseRedirect(reverse('asset:create'))
