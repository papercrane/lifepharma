from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'^$', views.index, name="list"),
    url(r'^create/$', views.create, name="create"),
    url(r'^code/settings/$', views.code_settings, name='code_settings'),
    url(r'^mail/settings/$', views.mail_settings, name='mail_settings'),
    url(r'^(?P<pk>[^/]+)/', include([
        url(r'^edit/$', views.edit, name="edit"),
        url(r'^detail/$', views.detail, name='detail'),
        url(r"^detail.pdf/$", views.InventoryPurchaseorderPDFView.as_view(), name="download-pdf"),
        url(r'^mail/$', views.purchase_order_mail, name='mail'),
        url(r'^delete/$', views.delete, name="delete"),

    ]))
]
