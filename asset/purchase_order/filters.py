import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from .models import PurchaseOrder


class PurchaseOrderFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))

    total_amount = django_filters.NumberFilter(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    total_amount__gt = django_filters.NumberFilter(name='total_amount', lookup_expr='gt',
                                                   widget=forms.NumberInput(attrs={'class': 'form-control'}))
    total_amount__lt = django_filters.NumberFilter(name='total_amount', lookup_expr='lt',
                                                   widget=forms.NumberInput(attrs={'class': 'form-control'}))

    supplier = django_filters.CharFilter(label="Supplier", method='filter_by_supplier')

    status = django_filters.ChoiceFilter(choices=PurchaseOrder.STATUSES,
                                         widget=forms.Select(attrs={'class': 'form-control'}))

    # created_at = django_filters.DateTimeFromToRangeFilter(
    #     name='created_at', widget=widgets.RangeWidget(attrs={'hidden': 'hidden'}))
    order_by_created_at = django_filters.ChoiceFilter(label="Created date", name="created_at", method='order_by_field', choices=(
        ('created_at', 'Ascending'), ('-created_at', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    order_by_total_amount = django_filters.ChoiceFilter(label="amount", name="total_amount", method='order_by_field', choices=(
        ('total_amount', 'Ascending'), ('-total_amount', 'Descending'),), widget=forms.Select(attrs={'class': 'form-control'}))

    ordering = django_filters.OrderingFilter(
        fields=(
            ('total_amount', 'total_amount'),
            ('created_at', 'latest'),
        ),
    )

    class Meta:
        model = PurchaseOrder
        fields = ['code', 'total_amount', 'supplier', 'status']

    def filter_by_contact(self, queryset, name, value):
        return queryset.filter(
            Q(supplier_contacts__name__icontains=value) | Q(supplier_contacts__supplier__name__icontains=value)
        )

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)
