from django import forms
from django.forms import ModelForm
from .models import PurchaseOrder, MailSetting, PurchaseOrderItem as Item, PurchaseOrderCodeSetting as CodeSetting
from asset.models import Asset as Product


class PurchaseOrderForm(ModelForm):
    class Meta:
        model = PurchaseOrder
        widgets = {
            "code": forms.TextInput(attrs={'class': "form-control1", 'required': "required", 'id': "code"}),
            "status": forms.Select(
                attrs={'class': "form-control1", "data-toggle": "select", 'id': "status"}
            ),

            # "tax": forms.NumberInput(attrs={'class': "form-control1", 'required': "required", 'id': "tax"}),
            "shipping_charge": forms.NumberInput(
                attrs={'class': "form-control1", 'required': "required", 'id': "shipping_charge"}
            ),
            "issued_date": forms.DateInput(attrs={'class': "form-control1", 'id': "issued_date"}),
        }
        fields = ['code', 'status','mode_of_delivery', 'shipping_charge', 'issued_date']


class PurchaseOrderEditForm(ModelForm):
    class Meta:
        model = PurchaseOrder
        widgets = {
            "status": forms.Select(attrs={'class': "form-control1", "data-toggle": "select", 'id': "status"}),

            "mode_of_delivery": forms.Select(
                attrs={'class': "form-control1", 'data-toggle': "select", 'id': "mode_of_delivery"}),
            # "tax": forms.NumberInput(attrs={'class': "form-control1", 'required': "required", 'id': "tax"}),
            "shipping_charge": forms.NumberInput(
                attrs={'class': "form-control1", 'required': "required", 'id': "shipping_charge"}),
            "issued_date": forms.DateInput(attrs={'class': "form-control1", 'id': "issued_date"}),
        }
        fields = ['status', 'mode_of_delivery', 'shipping_charge', 'issued_date']


class ItemForm(ModelForm):

    # asset = forms.ModelChoiceField(
    #     queryset=Product.fixed_asset,
    #     widget=forms.Select(attrs={'class': "form-control1 product", 'required': "required", 'id': "product"}),
    # )

    class Meta:
        model = Item
        widgets = {
            # "raw_material": forms.Select(
            #     attrs={'class': "form-control1 product", 'required': "required", 'id': "product"}),
            "price": forms.NumberInput(
                attrs={'class': "form-control1 price", 'id': "price"}),
            # "discount": forms.NumberInput(
            #     attrs={'class': "form-control1 discount", 'id': "discount"}),
            # "discount_type": forms.Select(
            #     attrs={'class': "form-control1 discount_type", 'data-toggle': "select", 'id': "discount_type"}),
            # "after_discount": forms.NumberInput(
            #     attrs={'class': "form-control1 discount_type", 'required': "required", 'id': "after_discount"}),
            "ordered_quantity": forms.NumberInput(attrs={'class': "form-control1 ordered_quantity", 'id': "ordered_quantity"}),
            # # "delivered_quantity": forms.NumberInput(attrs={'class': "form-control1", 'id': "delivered_quantity "}),
            "expected_date": forms.DateInput(
                attrs={'class': "form-control1 expected_date", 'id': "expected_date"}),
            "total": forms.NumberInput(
                attrs={'class': "form-control1 total", 'id': "total"}),
            # "tax": forms.NumberInput(attrs={'class': "form-control1 tax", 'id': "tax"}),
            # "status": forms.Select(attrs={'class': "form-control1", 'id': "status"}),
            # "batch": forms.Select(attrs={'class': "form-control1", 'id': "batch"}),
        }

        fields = ['asset', 'price', 'expected_date', 'total', 'ordered_quantity']


class CodeSettingForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


class MailSettingsForm(ModelForm):
    class Meta:
        model = MailSetting
        widgets = {
            "subject": forms.TextInput(attrs={'class': "form-control", 'id': "subject", 'required': "required"}),
            "content": forms.Textarea(attrs={'class': "form-control", 'id': "content", 'required': "required"}),
            "cc": forms.Select(attrs={'class': "form-control", 'id': "cc"}),
            "group_cc": forms.SelectMultiple(
                attrs={'class': "form-control", "data-toggle": "select", "id": "group_cc", 'multiple': "multiple"}),
        }
        fields = ['subject', 'content', 'cc', 'group_cc']
