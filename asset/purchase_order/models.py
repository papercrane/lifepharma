from django.db import models
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting, DISCOUNT_CHOICES, DISCOUNT_PERCENTAGE
from asset.models import Asset
# Create your models here.


class PurchaseOrder(ErpAbstractBaseModel):

    STATUS_OPEN = 'open'
    STATUS_ACCEPTED = 'accepted'
    STATUSES = (
        (STATUS_OPEN, "OPEN"),
        (STATUS_ACCEPTED, "ACCEPTED"),
    )

    def newCode():
        return PurchaseOrderCodeSetting.new_code()

    code = models.CharField(max_length=20, unique=True, default=newCode)
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_OPEN)
    total_amount = models.DecimalField(max_digits=20, decimal_places=2, default=0, null=True, blank=True)
    issued_date = models.DateTimeField(null=True, blank=True)
    # tax = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    shipping_charge = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    issued_by = models.ForeignKey('employee.Employee', on_delete=models.CASCADE,
                                  null=True, blank=True, related_name="asset_purchase_order_issued_by")

    mode_of_delivery = models.ForeignKey(
        'company.ModeOfDelivery', on_delete=models.CASCADE, related_name="asset_purchase_orders_mode_of_delivery", null=True, blank=True)
    send_mail = models.BooleanField(default=False)

    class Meta():
        app_label = 'asset'

    def __str__(self):
        return self.code

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            PurchaseOrderCodeSetting.incrementCountIndex()

        return super(PurchaseOrder, self).save(*args, **kwargs)

    def setAmount(self):

        products = self.order_items
        self.total_amount = 0
        for product in products.all():
            self.total_amount = self.total_amount + product.getPrice() * product.ordered_quantity

        # self.amount = amount
        if self.shipping_charge:
            self.total_amount = self.total_amount + self.shipping_charge
        # if self.tax:
        #     self.total_amount = self.total_amount + self.tax

        # self.save()
        return self


class PurchaseOrderItem(ErpAbstractBaseModel):

    @staticmethod
    def roundOff(price):
        return round(price, 2)

    PENDING = 'pending'
    INTRANSIT = 'intransit'
    PARTIALLY_DELIVERED = 'partially delivered'
    DELIVERED = 'delivered'
    CHOICES = (
        (PENDING, 'Pending'),
        (INTRANSIT, 'Intransit'),
        (PARTIALLY_DELIVERED, 'partially delivered'),
        (DELIVERED, 'Delivered'),
    )

    asset = models.ForeignKey(
        Asset, on_delete=models.CASCADE, related_name="asset_po_items"
    )
    # consumable = models.ForeignKey(
    #     Asset, on_delete=models.CASCADE, related_name="cm_po_items"
    # )

    price = models.DecimalField(max_digits=20, decimal_places=2)
    # discount = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    # discount_type = models.CharField(max_length=20, choices=DISCOUNT_CHOICES, default=DISCOUNT_PERCENTAGE)
    # after_discount = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    ordered_quantity = models.PositiveIntegerField(null=True, blank=True)
    # damaged_quantity = models.PositiveIntegerField(null=True, blank=True, default=0)
    # delivered_quantity = models.PositiveIntegerField(null=True, blank=True, default=0)
    # balance_quantity = models.PositiveIntegerField(null=True, blank=True, default=0)
    expected_date = models.DateField(null=True, blank=False)
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    # status = models.CharField(max_length=30, choices=CHOICES, default=PENDING)
    # tax = models.DecimalField(max_digits=20, decimal_places=2, default=0)

    purchase_order = models.ForeignKey(PurchaseOrder, on_delete=models.CASCADE, related_name="order_items")
    # batch = models.ForeignKey(Batch, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('asset', 'purchase_order')
        app_label = 'asset'

    def __str__(self):
        return str(self.asset) + str(self.purchase_order)

    def save(self, *args, **kwargs):
        self.setTotalPrice()
        return super(PurchaseOrderItem, self).save(*args, **kwargs)

    # def setAfterDiscount(self):
    #     after_discount = 0
    #     if self.discount_type == DISCOUNT_PERCENTAGE:
    #         after_discount = self.price * (1 - self.discount / 100)
    #     else:
    #         after_discount = self.price - self.discount
    #     self.after_discount = self.roundOff(after_discount)
    #     return self

    def setTotalPrice(self):
        # self.setAfterDiscount()
        self.total = self.getPrice() * self.ordered_quantity
        return self

    def getPrice(self):
        return self.price


class PurchaseOrderCodeSetting(BaseCodeSetting):

    class Meta():
        app_label = 'asset'


class MailSetting(models.Model):
    subject = models.CharField(max_length=20, null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    cc = models.ForeignKey("employee.Employee", related_name="asset_mail_cc", null=True, blank=True)
    group_cc = models.ManyToManyField("employee.Employee", related_name="asset_groupcc_mail_settings", blank=True)

    class Meta():
        app_label = 'asset'
