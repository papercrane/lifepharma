from django.db.models import F
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.forms import inlineformset_factory
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone
from erp_core.pagination import paginate
from easy_pdf.views import PDFTemplateView
from easy_pdf.rendering import render_to_pdf
from asset.stock.models import Stock
from .models import Asset as Product
from location.models import Location
from .forms import PurchaseOrderForm, ItemForm, PurchaseOrderEditForm, CodeSettingForm, MailSettingsForm
from .models import PurchaseOrder, MailSetting, PurchaseOrderItem as Item, PurchaseOrderCodeSetting as CodeSetting
from .filters import PurchaseOrderFilter
from django.db import IntegrityError, transaction


def index(request):
    # print(request.GET)

    f = PurchaseOrderFilter(request.GET, queryset=PurchaseOrder.objects.all().order_by("-created_at"))
    # purchaseorders = InventoryPurchaseOrder.objects.all()

    purchaseorders, pagination = paginate(request, queryset=f.qs, per_page=15, default=True)
    context = {

        'purchaseorders': purchaseorders,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "asset/purchase_order/list.html", context)


@transaction.atomic
def create(request):

    PurchaseItemInlineFormSet = inlineformset_factory(
        PurchaseOrder, Item, form=ItemForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    purchase_item_formset = PurchaseItemInlineFormSet(request.POST or None)
    locations = Location.objects.all()
    purchase_order_form = PurchaseOrderForm(request.POST or None)
    if request.method == "POST":
        if purchase_order_form.is_valid() and purchase_item_formset.is_valid():
            try:
                with transaction.atomic():

                    purchase_order = purchase_order_form.save(commit=False)
                    purchase_order.issued_by = request.user.employee
                    purchase_order.issued_date = timezone.now()
                    purchase_order.save()

                    items = purchase_item_formset.save(commit=False)
                    for item in items:
                        item.purchase_order = purchase_order
                        # item.setAfterDiscount()
                        item.save()
                        # set sales order foreign key
                        purchase_order.setAmount().save()

                    # stock create
                    cur_location = request.POST['location']
                    location = Location.objects.get(id=cur_location)
                    Stock.objects.create(
                        code=Stock.newCode(), date_time=timezone.now(), location=location, purchase_order=purchase_order
                    )
                    return redirect(reverse('asset:purchase_order:detail', args=[purchase_order.id]))
            except IntegrityError:
                messages.add_message(request, messages.SUCCESS,'Please make sure that stock code setting is done or not')
                return redirect(reverse('asset:purchase_order:create'))

    context = {
        'purchaseorder_form': purchase_order_form,
        'purchase_item_formset': purchase_item_formset,
        'form_url': reverse_lazy('asset:purchase_order:create'),
        'type': "Create",
        'locations': locations,
        'products': Product.fixed_asset.all(),

    }

    return render(request, "asset/purchase_order/edit.html", context)


@transaction.atomic
def edit(request, pk=None):
    purchase_order = get_object_or_404(PurchaseOrder, id=pk)

    purchase_order_form = PurchaseOrderEditForm(request.POST or None, instance=purchase_order)

    PurchaseItemInlineFormSet = inlineformset_factory(
        PurchaseOrder, Item, form=ItemForm, extra=0, can_delete=True, min_num=1, validate_min=True)
    purchase_item_formset = PurchaseItemInlineFormSet(request.POST or None, instance=purchase_order)

    if request.method == "POST":

        if purchase_order_form.is_valid() and purchase_item_formset.is_valid():
            purchase_order_item = purchase_order_form.save(commit=False)
            purchase_order_item.save()
            purchase_item_formset.save()
            purchase_order_item.setAmount().save()

            return redirect(reverse('asset:purchase_order:detail', args=[purchase_order.id]))

    context = {
        'purchaseorder_form': purchase_order_form,
        'purchase_item_formset': purchase_item_formset,
        'form_url': reverse_lazy('asset:purchase_order:edit', args=[purchase_order.pk]),
        'type': "Edit",
        'purchase_order': purchase_order,
        'products': Product.fixed_asset.all(),
    }
    return render(request, "asset/purchase_order/edit.html", context)


def detail(request, pk):
    purchase_order = get_object_or_404(PurchaseOrder, id=pk)
    items = purchase_order.order_items
    context = {
        'purchaseorder': purchase_order,
        'items': items,

    }
    return render(request, "asset/purchase_order/detail.html", context)


@transaction.atomic
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            return redirect(reverse('asset:purchase_order:list'))

    context = {
        'code_form': code_form,
    }

    return render(request, "asset/purchase_order/code_setting.html", context)


def mail_settings(request):

    if MailSetting.objects.exists():
        mail_form = MailSettingsForm(request.POST or None, instance=MailSetting.objects.last())
    else:
        mail_form = MailSettingsForm(request.POST or None)

    if request.method == "POST":

        if mail_form.is_valid():
            mail_form.save()
            return HttpResponseRedirect(reverse('asset:purchase_order:list'))

    context = {

        'mail_form': mail_form,
    }

    return render(request, "asset/purchase_order/mail_settings.html", context)


class InventoryPurchaseorderPDFView(PDFTemplateView):

    template_name = "asset/purchase_order/purchaseorder_pdf.html"

    def get_context_data(self, **kwargs):
        pk = kwargs.get('pk', False)
        purchase_order = get_object_or_404(PurchaseOrder, id=pk)
        items_pro = purchase_order.order_items
        return super(InventoryPurchaseorderPDFView, self).get_context_data(
            pagesize="A4",
            title="Purchase Order",
            purchaseorder=PurchaseOrder.objects.get(pk=pk),
            items=items_pro,
            # products=PurchaseOrderItems.objects.all(),
        )


def purchase_order_mail(request, pk):
    mail_list = []
    purchase_order = PurchaseOrder.objects.get(pk=pk)
    products = purchase_order.order_items
    mail_settings = MailSetting.objects.last()
    if request.method == "POST":
        source = request.POST.get('source', False)

        destination = request.POST.get('destination', False)
        subject = request.POST.get('subject', False)
        content = request.POST.get('message', False)
        cc = request.POST.get('cc', False)

        if source and destination and subject and content:

            if MailSetting.objects.exists():
                group_cc = MailSetting.objects.last().group_cc.all()
                for gcc in group_cc:
                    mail_list.append(gcc.user.email)

            # attachement = request.POST.get('attachement', False)

            attachement_content = {
                'purchaseorder': purchase_order,
                'products': products
            }
            pdf_attachment = render_to_pdf("asset/purchase_order/raw_material_purchase_order_pdf.html",
                                           attachement_content, encoding='utf-8')
            # send_mail(source,destination,subject,content,attachement)
            htmly = get_template('asset/purchase_order/email.html')

            d = Context({'purchaseorder': purchase_order, 'content': content})

            html_content = htmly.render(d)
            try:
                if not MailSetting.objects.exists():
                    msg = EmailMultiAlternatives(subject, "Purchaseorder", source, [destination, cc])
                else:
                    msg = EmailMultiAlternatives(
                        subject, "Purchaseorder", source, [destination], cc=[cc], bcc=['mail_list']
                    )
                msg.attach_alternative(html_content, "text/html")
                msg.attach("purchase-order.pdf", pdf_attachment, 'application/pdf')
                msg.send()

            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            messages.add_message(request, messages.ERROR, "Purchase order send successfully")
            purchase_order.send_mail = True
            purchase_order.save()

            return redirect(reverse('asset:purchase_order:detail', args=[purchase_order.id]))

    context = {

        'purchaseorder': purchase_order,
        'mail_settings': mail_settings,
    }
    return render(request, "asset/purchase_order/mail.html", context)
# Generating pdf


def delete(request, pk):
    if request.method == "POST":
        purchaseorder = get_object_or_404(PurchaseOrder, id=pk)
        # print("delete")
        purchaseorder.delete()
    return HttpResponseRedirect(reverse('asset:purchase_order:list'))
