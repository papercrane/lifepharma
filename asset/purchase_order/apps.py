import os
from django.apps import AppConfig


class PurchaseOrderConfig(AppConfig):
    name = 'asset.purchase_order'
    label = "asset_purchase"
    path = os.path.dirname(os.path.abspath(__file__))
