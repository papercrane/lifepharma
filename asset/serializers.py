from django.conf import settings
from rest_framework import serializers
from .models import AssetTransaction
from customer.serializers import CustomerSerializer


class TransactionSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    date_and_time = serializers.SerializerMethodField('is_named_bar')

    def is_named_bar(self, transaction):
        return transaction['date_and_time'].strftime('%Y-%m-%d')

    class Meta:
        model = AssetTransaction
        fields = ('customer', 'date_and_time')
