from django.conf.urls import include, url
from asset import asset, asset_type
from .stock.urls import stock_patterns

app_name = "asset"

asset_type_patterns = ([
    url(r'^asset_type/', include([

        url(r'^create/$', asset_type.create, name="create"),
        url(r'^asset_category/$', asset_type.asset_category, name="asset_category"),

    ])),

], 'asset_type')


urlpatterns = [

    url(r'^asset/', include([
        url(r'^index/$', asset.index, name="list"),
        url(r'^dashboard/$', asset.dashboard, name="dashboard"),
        url(r'^create/$', asset.create, name="create"),
        url(r'^customer/asset/create/$', asset.customer_asset_create, name="customer_asset_create"),
        url(r'^customer/asset/(?P<pk>[^/]+)/edit/$', asset.customer_asset_edit, name="customer_asset_edit"),
        url(r'^(?P<pk>[^/]+)/edit/$', asset.edit, name="edit"),
        url(r'^(?P<pk>[^/]+)/detail/$', asset.detail, name="detail"),
        url(r'^(?P<pk>[^/]+)/delete/$', asset.delete, name="delete"),
        url(r'^stocks/', include(stock_patterns, namespace="stock")),
        url(r'^code/settings/$', asset.code_settings, name='code_settings'),
        url(r'^(?P<barcode>[^/]+)/transaction/$', asset.asset_transaction, name="asset_transaction"),

    ])),
    url(r'^asset/', include(asset_type_patterns)),
    url(r'^transaction/(?P<pk>[^/]+)/detail/$', asset.transaction_detail, name="transaction_detail"),
    url(r'^customer/transactions/(?P<selectedDate>[^/]+)/$', asset.customer_transaction, name="customer_transaction"),
    url(r'^customer/transactions/$', asset.customer_transaction, name="customer_transaction"),
    url(r'^customer/assets/(?P<date>[^/]+)/(?P<customer>[^/]+)/$', asset.customer_asset, name="customer_asset"),
    url(r'^asset/change/status/(?P<pk>[^/]+)/$', asset.changeStatus, name="changeStatus"),
    url(r'^employee/assets/$', asset.employee_assets, name="employee_assets"),
    url(r'^return/asset/(?P<pk>[^/]+)/$', asset.return_asset, name="return_asset"),


]

