import datetime

from sitetree.utils import item, tree
from django.utils import timezone

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Asset',
            'asset:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="gavel",
            children=[

                # item(
                #     "List",
                #     'asset:list',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="list",
                # ),
                # item(
                #     'Store',
                #     '#',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="list",
                # ),

                ]
            # access_by_perms=['project.view_projects']
        ),
        # item(
        #     'Task',
        #     'project:task:list project.id',
        #     in_menu=True,
        #     in_sitetree=True,
        #     access_loggedin=True,
        #     icon_class="clipboard",
        # )
    ]),

    tree('sidebar_technician', items=[
        # Then define items and their children with `item` function.
        item(
            'Transactions',
            'asset:customer_transaction',
            '',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="stethoscope",

        ),

    ]),


    tree('sidebar_store_keeper', items=[

    item(

        'Assets',
        'asset:list',
        in_menu=True,
        in_sitetree=True,
        access_loggedin=True,
        icon_class="list",
    ),
    item(
        'Store',
        'asset:employee_assets',
        '',
        in_menu=True,
        in_sitetree=True,
        access_loggedin=True,
        icon_class="stethoscope",

    ),

    ]),


    # ... You can define more than one tree for your app.
)
