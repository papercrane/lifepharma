from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render


# Create your views here.


@login_required
def dashboard(request):

    return HttpResponseRedirect("employee/" + str(request.user.employee.code) + "/profile")
