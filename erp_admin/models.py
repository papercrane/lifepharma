from django.db import models
# Create your models here.
# Suppose you have `myapp` application.
# In its `models.py` you define your customized models.
from sitetree.models import TreeItemBase


class MyTreeItem(TreeItemBase):
    """And that's a tree item model with additional `css_class` field."""
    css_class = models.CharField('Tree item CSS class', max_length=50)
    icon_class = models.CharField('Tree item ICON class', max_length=50)
