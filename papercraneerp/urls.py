from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from sitetree.sitetreeapp import compose_dynamic_tree, register_dynamic_trees
from erp_admin import views as erpAdminViews


# from employee import views as employeeViews
urlpatterns = [
    url(r'^$', erpAdminViews.dashboard),
    # url(r'^api-token-auth/', views.obtain_auth_token),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^', include('employee.urls', namespace="employee")),
    url(r'^', include('customer.urls', namespace="customer")),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^', include('location.urls', namespace="location")),
    url(r'^company/', include('company.urls', namespace="company")),
    url(r'^', include('asset.urls', namespace="asset")),




] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# urlpatterns = static(settings.MEDIA_URL, view=never_cache(serve_static),
#                          document_root=settings.MEDIA_ROOT) + urlpatterns
admin.site.site_header = 'Jawdrop Administration'




register_dynamic_trees(

    compose_dynamic_tree('erp_admin'),

    # or gather all the trees from `books` and attach them to `main` tree root,
    compose_dynamic_tree('employee'),
    compose_dynamic_tree('customer'),
    compose_dynamic_tree('asset'),





)


